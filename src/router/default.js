const path = require('path');
const cons = require('consolidate');

module.exports = function(app, express){
    const router = express.Router();

    app.engine('html', cons.swig)
    app.set('public', path.join(__dirname, '../../../public'));
    app.set('view engine', 'html');
    app.use(
        express.static(__dirname + '../../../public')
    );
    app.use(function(req, res, next) {
        req.setTimeout(0);
        next();
    })
    app.use('/', router);

    router.get('/', (req, res) => {
        res.type('html');
        let renderedViews = "";

        app.render(path.join(__dirname + '../../../public', 'header'), 
            (err, html) => {
            renderedViews += html;
            app.render(path.join(__dirname + '../../../public', 'home'), 
                (err, html) => {
                    renderedViews += html;
                    res.send(renderedViews);
                }
            );
            }
        );
    });

    router.get('/home', (req, res) => {
        res.redirect('/');
    });

    router.get('/etalonnage', (req, res) => {
        res.type('html');
        let renderedViews = "";

        app.render(path.join(__dirname + '../../../public', 'header'), 
            (err, html) => {
            renderedViews += html;
            app.render(path.join(__dirname + '../../../public', 'etalonnage'), 
                (err, html) => {
                    renderedViews += html;
                    res.send(renderedViews);
                }
            );
            }
        );
    });

    router.get('/gestionRelay', (req, res) => {
        res.type('html');
        let renderedViews = "";

        app.render(path.join(__dirname + '../../../public', 'header'), 
            (err, html) => {
            renderedViews += html;
            app.render(path.join(__dirname + '../../../public', 'gestionRelay'), 
                (err, html) => {
                    renderedViews += html;
                    res.send(renderedViews);
                }
            );
            }
        );
    });

    router.get('/paramArduino', (req, res) => {
        res.type('html');
        let renderedViews = "";

        app.render(path.join(__dirname + '../../../public', 'header'), 
            (err, html) => {
            renderedViews += html;
            app.render(path.join(__dirname + '../../../public', 'paramArduino'), 
                (err, html) => {
                    renderedViews += html;
                    res.send(renderedViews);
                }
            );
            }
        );
    });

    router.get('/courbe', (req, res) => {
        res.type('html');
        let renderedViews = "";

        app.render(path.join(__dirname + '../../../public', 'header'), 
            (err, html) => {
            renderedViews += html;
            app.render(path.join(__dirname + '../../../public', 'courbe'), 
                (err, html) => {
                    renderedViews += html;
                    res.send(renderedViews);
                }
            );
            }
        );
    });

    router.get('/log', (req, res) => {
        res.type('html');
        let renderedViews = "";

        app.render(path.join(__dirname + '../../../public', 'header'), 
            (err, html) => {
            renderedViews += html;
            app.render(path.join(__dirname + '../../../public', 'log'), 
                (err, html) => {
                    renderedViews += html;
                    res.send(renderedViews);
                }
            );
            }
        );
    });

    router.get('/status', (req, res) => {
        let processTime = {
          up_time: Math.floor(process.uptime())
        };
        
        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(processTime));
      });

    router.get('/testco2', (req, res) => {
        const http = require('http');
        const fs = require('fs');
        

        let fichier = fs.readFileSync('configCO2.json');
        let posteInfo = JSON.parse(fichier);

        http.get('http://' + posteInfo.ipMaster + ':3100/getCO2/' + posteInfo.room, (resp) => {
            let data = '';

            resp.on('data', (chunk) => {
                data += chunk;
            })

            resp.on('end', () => {
                res.statusCode = 200;
                res.setHeader('Content-Type', 'application/json');
                res.end(JSON.stringify(data));
            })
        });


    });
}