const path = require('path');
const cons = require('consolidate');
const co2Manager = require('../master/co2Manager');


module.exports = function(app, express){
    const router = express.Router();

    app.engine('html', cons.swig)
    app.set('public', path.join(__dirname, '../../../public'));
    app.set('view engine', 'html');
    app.use(
        express.static(__dirname + '../../../public')
    );
    app.use(function(req, res, next) {
      req.setTimeout(0);
      next();
  })
    app.use('/', router);

    app.get('/getCO2/:room', async (req, res) => {
        let co2 = await co2Manager.getCo2(req.params.room);

        res.statusCode = 200;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(co2));
    })
}