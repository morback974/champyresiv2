// info a envoyer au client
// suiviProcess:"",
// suiviSousProcess:"",
const {delay, arrondi, hToMs} = require('./util/utilitaire');
const client = require('./client');
let timeInterval = require('../bdd/timeInterval');
let actionAir = require('../bdd/actionAir');
let actionHum = require('../bdd/actionHum');
let {logger} = require('../src/util/logger');
const arduino = require('./arduino/arduino');

let arduinoBoard = 10;
let continuegestion = true;

async function launchGestion(){
    client.addElemClient("Lancement Gestion Champignon", "suiviProcess");
    console.log("Lancement Gestion Champignon");

    affichageTempRestant(30, "Chargement de l application");
    await delay(31, "seconde");

    arduinoBoard = require("./arduino/arduino").arduino;

    while(continuegestion){
        client.addElemClient("Lancement Gestion Temperature Air", "suiviProcess");

        await gestionTemperatureAir()
        .catch((err) => {
            console.log("Error lors de la gestion de temperature");
            console.error(err);
        });

        await timeInterval.getGestionHumidite().then(async (val) => {
            let lastGestionHumidite = Date.parse(val);
            
            if(Date.now() - lastGestionHumidite > hToMs(1)){
                client.addElemClient("Lancement Gestion Humidite", "suiviProcess");
                arduinoBoard.suiviDelayGestionHum();
                await gestionHumidite()
                .catch((err) => {
                    console.log("Error lors de la gestion de humidite");
                    console.error(err);
                });

                timeInterval.initGestionHumidite();

                // GESTION CO2
                gestionCo2()
                .catch((err) => {
                    console.log("Error lors de la gestion de CO2");
                    console.error(err);
                });
            }
        }).catch(() => {
            // nada
        });

        checkingProblem();

        client.addElemClient("Attente Prochain Cycle", "suiviProcess");
        affichageTempRestant(300, "Delay entre gestion Champignon");
        await delay(5, "minute");
    }
}

async function gestionCo2(){
    return new Promise(async (resolve, reject) => {
        const http = require('http');
        const fs = require('fs');
        
        let fichier = fs.readFileSync('configCO2.json');
        let posteInfo = JSON.parse(fichier);

        await new Promise((resolve, reject) => {
                http.get('http://' + posteInfo.ipMaster + ':3100/getCO2/' + posteInfo.room, (resp) => {
                    let data = '';
        
                    resp.on('data', (chunk) => {
                        data += chunk;
                        //console.log(data)
                    })
        
                    resp.on('end', async () => {
                        data = JSON.parse(data);
                        client.addElemClient(data, "statusCo2");
                        if(data){
                            console.log("fin de Co2");
                            arduinoBoard.setTauxCo2(data);
                            let consigneCo2 = await arduinoBoard.getConsigneCo2();

                            if(data + 200 > consigneCo2){
                                arduinoBoard.setDureeVariateurOFF(-60);
                                console.log("CO2 > consigne : Diminue le temps OFF");
                            }else if(data - 200 < consigneCo2){
                                arduinoBoard.setDureeVariateurOFF(60);
                                console.log("CO2 < consigne : Augmente le temps OFF");
                            }

                            resolve();
                        }
        
                    })
                }).on("error", (err) => {
                    console.log(err);
                    reject("Acces au Serveur Master impossible (Mesure Co2)");
                });
        });
    }).catch((err) => {
        console.log(err);
    })
}

async function gestionTemperatureAir(){
    console.log("Gestion Temperature air");
    let temperatureAir = await arduinoBoard.getTemperatureAir(true);
    await arduinoBoard.secureMesureAir(temperatureAir);
    
    let deltaTemp = temperatureAir - await arduinoBoard.getConsigneAir();
    logger.info("Gestion Temperature Air", {deltaTemp:arrondi(deltaTemp)});

    return new Promise((resolve, reject) => {
        let dureeAction = dureeRegulationAir(deltaTemp);

        if(dureeAction > 0){
            if(deltaTemp > 0){
                affichageTempRestant(dureeAction, "Temperature > consigne, Ouverture Vanne Air");

                arduinoBoard.ouvrirVanneAir(dureeAction, false)
                .then(() => {
                    actionAir.saveAction(arduinoBoard, {dureeAction:dureeAction});
                    resolve();
                });
            }else{
                affichageTempRestant(dureeAction, "Temperature < consigne, Fermeture Vanne Air");

                arduinoBoard.fermerVanneAir(dureeAction, false)
                .then(() => {
                    actionAir.saveAction(arduinoBoard, {dureeAction:dureeAction});
                    resolve();
                })
            }
        }else{
            client.addElemClient("Temperature ~= consigne, Aucune Action", "suiviSousProcess");
            actionAir.saveAction(arduinoBoard, {dureeAction:0});
            resolve();
        }
    });
}

async function gestionHumidite(){
    console.log("Gestion Humidite");

    if(arduinoBoard.getNbJour() < 6){
        // todo 3 fois par jour
        //client.addElemClient(24/3, "tempsFermetureBrume");

        await new Promise((resolve, reject) => {
            timeInterval.getTempsBrumeInterval().then((val) => {
                let evolConsigne = Date.parse(val);
    
                if(Date.now() - evolConsigne > hToMs(4)){
                    let dureeAction = 120;
                    //client.addElemClient(dureeAction, "tempsOuvertureBrume");
                    affichageTempRestant(dureeAction, "Jour inferieure à 6, Active Brume");
                    logger.info("Gestion Temperature Humidite avant 6eme jour", {dureeAction:"120 sec"});

                    // arduinoBoard.activerBrume(dureeAction, false)
                    // .then(() => {
                    //     actionHum.saveAction(arduinoBoard, {brume: dureeAction});
                    //     resolve();
                    // }).catch((info) => {
                    //     reject(info);
                    // });
    
                    timeInterval.initTempsBrume();
                    resolve();
                }
            }).catch(() => {
                // nada
            })
        });

        return true;
    }else{
        client.addElemClient("Mesure Humidite", "suiviSousProcess");
        let deltaHum = await arduinoBoard.getDeltaHumidite(true);
        logger.info("Gestion Temperature Humidite", {deltaHum:arrondi(deltaHum)});
        
        return new Promise((resolve, reject) => {
            if(deltaHum > 0){
                client.addElemClient("Humidite > consigne, Active Deshum", "suiviSousProcess");
                let dureeAction = dureeDesHum(deltaHum);
                //client.addElemClient(dureeAction, "tempsDeshum");
                affichageTempRestant(dureeAction, "Humidite > consigne, Active Deshum");
                actionHum.saveAction(arduinoBoard, {desHum: dureeAction});
                // arduinoBoard.activerDesHum(dureeAction, false)
                // .then(() => {
                //     actionHum.saveAction(arduinoBoard, {desHum: dureeAction});
                //     resolve();
                // }).catch((info) => {
                //     reject(info);
                // });

                resolve();
            }else{
                let dureeAction = dureeBrume(deltaHum);
                //client.addElemClient(dureeAction, "tempsOuvertureBrume");
                affichageTempRestant(dureeAction, "Humidite < consigne, Active Brume");
                actionHum.saveAction(arduinoBoard, {brume: dureeAction});
                // arduinoBoard.activerBrume(dureeAction, false)
                // .then(() => {
                //     actionHum.saveAction(arduinoBoard, {brume: dureeAction});
                //     resolve();
                // }).catch((info) => {
                //     reject(info);
                // });

                resolve();
            }
        })
    }

}

function dureeDesHum(deltaHum){
    let timerDesHum = 0;
    if(deltaHum > 3){
        timerDesHum = 30;
    }else if(deltaHum > 2){
        timerDesHum = 45;
    }else if(deltaHum > 1){
        timerDesHum = 60;
    }else if(deltaHum > 0.3){
        timerDesHum = 105;
    }

    return timerDesHum;
}

function dureeBrume(deltaHum){
    let tempsFermetureBrume = 0;
    
    if(deltaHum > 3){
        tempsFermetureBrume = 30;
    }else if(deltaHum > 2){
        tempsFermetureBrume = 45;
    }else if(deltaHum > 1){
        tempsFermetureBrume = 60;
    }else if(deltaHum > 0.3){
        tempsFermetureBrume = 105;
    }

    return tempsFermetureBrume;
}

function dureeRegulationAir(deltaTemp){
    let dureeAction = 0;

    if(deltaTemp > 1.5){
        dureeAction = 40;
    }else if(deltaTemp > 1){
        dureeAction = 15;
    }else if(deltaTemp > 0.5){
        dureeAction = 5;
    }else if(deltaTemp > 0.3){
        dureeAction = 2;
    }

    if(deltaTemp < -1.5){
        dureeAction = 40;
    }else if(deltaTemp < -1){
        dureeAction = 15;
    }else if(deltaTemp < -0.5){
        dureeAction = 5;
    }else if(deltaTemp < -0.3){
        dureeAction = 2;
    }

    return dureeAction;
}

function affichageTempRestant(dureeAction, message){
    let dureeActionRestante = dureeAction;

    let intMess = setInterval(() => {
        client.addElemClient(`${message} (temps restant ${dureeActionRestante--}/${dureeAction})`, "suiviSousProcess");
    }, 1000)

    setTimeout(() => {
        clearInterval(intMess);
        client.addElemClient("", "suiviSousProcess");
    }, dureeAction * 1000);
}

async function checkingProblem(){
    if(arduinoBoard.isObjectifAirActif()){
        await arduinoBoard.getPasAir().catch(async (objConsigneAir) => {
            arduinoBoard.setObjConsigneAirActif();
            logger.error("Rectification Objectif Consigne Air", {objConsigneAir: objConsigneAir});
            arduinoBoard.setObjConsigneAir(await arduinoBoard.getTemperatureAir(false));
        })
    }

    if(arduinoBoard.isObjectifHumActif()){
        await arduinoBoard.getPasHum().catch(async (objConsigneHum) => {
            arduinoBoard.setObjConsigneHumActif();
            logger.error("Rectification Objectif Consigne Hum", {objConsigneHum: objConsigneHum});
            arduinoBoard.setObjConsigneHum(90);
        })
    }
}

module.exports = {launchGestion: launchGestion, affichageTempRestant: affichageTempRestant}