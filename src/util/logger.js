const client = require('../client');

let logger = {
    debug: (message, details = "") => {
        client.sendLog(new Log("DEBUG", message, details));
    },
    
    info: (message, details = "") => {
        client.sendLog(new Log("INFO", message, details));
    },

    notice: (message, details = "") => {
        client.sendLog(new Log("NOTICE", message, details));
    },

    error: (message, details = "") => {
        client.sendLog(new Log("ERROR", message, details));
    }
}

class Log {
    constructor(niveau, message, details){
        this.date = new Date();
        this.niveau = niveau;
        this.message = message;
        this.details = details;
    }
}

function test(){
    setInterval(() => {
        logger.error("test", "test2");
        console.log("test");
    }, 1000);
}

module.exports = {
    logger:logger,
    test:test
}