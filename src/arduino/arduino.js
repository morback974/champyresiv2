let five = require('johnny-five');
const mysqlDataArduino = require('../../bdd/dataArduino');
let timeInterval = require('../../bdd/timeInterval');
let gestionAir = require('../../bdd/gestionAir');
let gestionHum = require('../../bdd/gestionHum');
let {logger} = require('../util/logger');

const {hToMs, calMoy, arrondi} = require('../util/utilitaire');

let gestionChampignon = require('../gestionChampignon');
const client = require('../client');

// definition pin carte arduino
let listPin = [];
let pinTempAir = "A2";
let pinTempHum = "A1";
let pinTempSec = "A0";
let pinTest = 7;
let valVariateur = 0;

let pinOnVanneAir = [43, 45];
let pinOffVanneAir = 43;

let pinVentilo = 41;

let pinDesHum = 6;
let pinBrume = 39;

let allPinToInit = [pinTempAir, pinTempHum, pinTempSec, 
                    pinOnVanneAir, pinOffVanneAir, pinVentilo, 
                    pinDesHum, pinBrume, pinTest];

const AIR = "air";
const HUM = "hum";
const SEC = "sec";

// met a jour la temperature Air
const NBMESURE = 20;

let listTemp = [];
listTemp[AIR] = [];
listTemp[SEC] = [];
listTemp[HUM] = [];

let tabSecuriteAir = [];

let timeBeforeGestionHum = 0;

// parametre les infos a envoyer au client
setInterval(async () => {
    client.addElemClient(arduino.getNbJour(), "nbJour");

    let tabTemp = [
        [await arduino.getTemperatureAir(false), "temperatureAir"],
        [await arduino.getTemperatureSec(false), "temperatureSec"],
        [await arduino.getTemperatureHum(false), "temperatureHum"]
    ];
    client.addElemClient(tabTemp);

    let tabEtal = [
        [arrondi(arduino.getEtalonnageAir()), "etalAir"],
        [arrondi(arduino.getEtalonnageSec()), "etalSec"],
        [arrondi(arduino.getEtalonnageHum()), "etalHum"]
    ];
    client.addElemClient(tabEtal);

    client.addElemClient(arrondi(await arduino.getConsigneAir()), "consigneAir", false);
    client.addElemClient(arrondi(dataArduino.consigneHum), "consigneHum", false);

    client.addElemClient(arrondi(dataArduino.objConsigneAir), "objConsigneAir");
    client.addElemClient(dataArduino.modifConsigneAir, "pasConsigneAir");
    client.addElemClient(dataArduino.consigneAirActif, "consigneAirActif");

    client.addElemClient(arrondi(dataArduino.objConsigneHum), "objConsigneHum");
    client.addElemClient(dataArduino.modifConsigneHum, "pasConsigneHum");
    client.addElemClient(dataArduino.consigneHumActif, "consigneHumActif");

    client.addElemClient(arrondi(dataArduino.objConsigneCo2), "objConsigneCo2");
    client.addElemClient(dataArduino.modifConsigneCo2, "pasConsigneCo2");
    client.addElemClient(dataArduino.consigneCo2Actif, "consigneCo2Actif");

    client.addElemClient(arrondi(dataArduino.moySec), "moySec");
    client.addElemClient(arrondi(dataArduino.moyHum), "moyHum");
    
    client.addElemClient(arrondi(dataArduino.etatVanneFroid), "etatVanneAir");
    
    client.addElemClient(arrondi(await arduino.getDeltaTempAir()), "coeff");
    client.addElemClient(arrondi(dataArduino.tauxHumidite), "tauxHumidite");

    client.addElemClient(arrondi(await arduino.getConsigneCo2()), "consigneCo2", false);
    client.addElemClient(arrondi(dataArduino.tauxCo2), "tauxCo2");
    client.addElemClient(timeBeforeGestionHum + " min", "delayBeforeHum");
}, 1000);

let dataArduino = {
    _id:"",
    temperatureAir:0,
    consigneAir:0,
    modifConsigneAir:0,
    objConsigneAir:0,
    consigneAirActif: false,
    tauxHumidite:0,
    consigneHum:0,
    modifConsigneHum:0,
    objConsigneHum:0,
    consigneHumActif: false,
    consigneCo2:2500,
    modifConsigneCo2:0,
    objConsigneCo2:0,
    consigneCo2Actif: false,
    dureeVariateurOFF:300,
    dureeAction:0,
    coeff:0,
    etatVanneFroid:0,
    moySec:0,
    moyHum:0,
    tempsDeshum:0,
    tempsOuvertureBrume:0,
    tempsFermetureBrume:0,
    dureeActivationBrume:120000,
    etalonageAir:0,
    etalonageHum:0,
    etalonageSec:0,
    tauxCo2: 0,
    nbJour:0,
    date:"",
    portUsed:getNamePort()
};

let arduino = {
    getTemperatureAir : async (takeMeasure = true) => {
        if(takeMeasure){
            return await getTemperatureByLabel(dataArduino.temperatureAir, dataArduino.etalonageAir, AIR);
        }else{
            return arrondi(dataArduino.temperatureAir + dataArduino.etalonageAir);
        }
    },

    setConsigneAir: (val) => {
        if(val > 10 && val < 40){
            dataArduino.consigneAir = val;
        }
    },

    getConsigneAir : () => {
        return new Promise((resolve, reject) => {
            if(isNaN(dataArduino.consigneAir) || dataArduino.consigneAir < 0 || dataArduino.consigneAir > 40){
                reject(dataArduino.consigneAir);
            }else{
                resolve(dataArduino.consigneAir);
            }
        });
    },

    setConsigneHum: (val) => {
        if(val > 80 && val < 100){
            dataArduino.consigneHum = val;
        }
    },

    getConsigneHum: () => {
        return new Promise((resolve, reject) => {
            if(isNaN(dataArduino.consigneHum) || dataArduino.consigneHum < 50 || dataArduino.consigneHum > 130){
                reject(dataArduino.consigneHum);
            }else{
                resolve(dataArduino.consigneHum);
            }
        });
    },

    getConsigneCo2: () => {
        return new Promise((resolve, reject) => {
            resolve(dataArduino.consigneCo2);
        })
    },

    getObjAir : () => {
        return new Promise(async (resolve, reject) => {
            if(dataArduino.objConsigneAir < 10 || dataArduino.objConsigneAir > 40){
                // rajout securite
                if(dataArduino.consigneAirActif){
                    dataArduino.consigneAirActif = false;
                    logger.error("Rectification Objectif Consigne Air", {objConsigneAir: dataArduino.objConsigneAir});
                    dataArduino.objConsigneAir = await arduino.getTemperatureAir(false);
                }
                reject(dataArduino.objConsigneAir);
            }else{
                resolve(dataArduino.objConsigneAir);
            }
        });
    },

    getObjHum : () => {
        return new Promise(async (resolve, reject) => {
            if(dataArduino.objConsigneHum < 60 || dataArduino.objConsigneHum > 130){
                // rajout securite
                reject("getObjHum", dataArduino.objConsigneHum);
            }else{
                resolve(dataArduino.objConsigneHum);
            }
        });
    },

    getPasAir : () => {
        return new Promise(async (resolve, reject) => {
            if(await arduino.getTemperatureAir(false) - dataArduino.modifConsigneAir < 0 || dataArduino.modifConsigneAir + await arduino.getTemperatureAir(false) > 30){
                let pasAir = dataArduino.modifConsigneAir;
                dataArduino.modifConsigneAir = 0;
                reject("getPasAir", pasAir);
            }else{
                resolve(dataArduino.modifConsigneAir);
            }
        });
    },

    getPasHum : () => {
        return new Promise(async (resolve, reject) => {
            if(await arduino.getTemperatureHum(false) - dataArduino.modifConsigneHum < 0 || dataArduino.modifConsigneHum + await arduino.getTemperatureHum(false) > 130){
                let pasHum = dataArduino.modifConsigneHum;
                dataArduino.modifConsigneHum = 0;
                reject("getPasHum", pasHum);
            }else{
                resolve(dataArduino.modifConsigneHum);
            }
        });
    },

    isObjectifAirActif: () => {
        return dataArduino.consigneAirActif;
    },

    setObjConsigneAir: (val = false) => {
        dataArduino.consigneAirActif = val;
    },

    isObjectifHumActif: () => {
        return dataArduino.consigneHumActif;
    },

    setObjConsigneHum: (val = false) => {
        dataArduino.consigneHumActif = val;
    },

    setObjConsigneAirActif: () => {
        dataArduino.consigneAirActif = !dataArduino.consigneAirActif;
    },

    setObjConsigneHumActif: () => {
        dataArduino.consigneHumActif = !dataArduino.consigneHumActif;
    },

    getDureeVariateurOFF: () => {
        return dataArduino.dureeVariateurOFF;
    },

    setDureeVariateurOFF: (val) => {
        let verifValOk = dataArduino.dureeVariateurOFF + val;

        if(verifValOk < 60){
            dataArduino.dureeVariateurOFF = 60;
        }else if(verifValOk > 600){
            dataArduino.dureeVariateurOFF = 600;
        }else{
            dataArduino.dureeVariateurOFF += val;
        }
    },

    getTemperatureSec:  async (takeMeasure = true) => {
        if(takeMeasure){
            return await getTemperatureByLabel(dataArduino.temperatureSec, dataArduino.etalonageSec, SEC);
        }else{
            return arrondi(dataArduino.temperatureSec + dataArduino.etalonageSec);
        }
    },

    getTemperatureHum:  async (takeMeasure = true) => {
        if(takeMeasure){
            return await getTemperatureByLabel(dataArduino.temperatureHum, dataArduino.etalonageHum, HUM);
        }else{
            return arrondi(dataArduino.temperatureHum + dataArduino.etalonageHum);
        }
    },

    getDeltaTempAir: async () => { 
        return await arduino.getTemperatureAir(false) - dataArduino.consigneAir;
    },

    getDeltaHumidite: async (makeMeasure = false) => {
        return await arduino.getTauxHumidite(makeMeasure) - dataArduino.consigneHum;
    },

    getTauxHumidite: (makeMeasure = true) => {
        if(makeMeasure){
            turnLow(pinVentilo);
            listPin[pinVentilo].isEnable = false;
    
            let intSec = null;
            let intHum = null;
    
            let mesureSec = [];
            let mesureHum = [];
    
            gestionChampignon.affichageTempRestant(30, "Allumage Ventilateur");
            setTimeout(() => {
                gestionChampignon.affichageTempRestant(90, "Mesure Temperature Seche");
                intSec = setInterval(async () => {
                    let temperatureSec = await arduino.getTemperatureSec()
                    mesureSec.push(temperatureSec);
                }, 1000);
        
                setTimeout(() => {
                    gestionChampignon.affichageTempRestant(90, "Mesure Temperature Seche et Humide");
                    intHum = setInterval(async () => {
                        let temperatureHum = await arduino.getTemperatureHum()
                        mesureHum.push(temperatureHum);
                    }, 1000);
                }, 91000)
            }, 30000);
    
            return new Promise((resolve, reject) => {
                // duree des mesure 3 min (180 000sec)
                setTimeout(() => {
                    clearInterval(intSec, intHum);
                    arduino.setMoySec(calMoy(mesureSec));
                    arduino.setMoyHum(calMoy(mesureHum));

                    turnHigh(pinVentilo);
                    listPin[pinVentilo].isEnable = true;
    
                    arduino.setTauxHumidite(calculHumidite(dataArduino.moySec, dataArduino.moyHum));

                    resolve(dataArduino.tauxHumidite);
                }, 210000);
            })    
        }else{
            return dataArduino.tauxHumidite;
        }
    },

    getNbJour: () => {
        return dataArduino.nbJour;
    },

    getEtatVanneFroid: () => {
        return dataArduino.etatVanneFroid;
    },

    getEtalonnageHum: () => {
        return dataArduino.etalonageHum;
    },

    getEtalonnageSec: () => {
        return dataArduino.etalonageSec;
    },

    getEtalonnageAir: () => {
        return dataArduino.etalonageAir;
    },

    increaseConsigneAir: (pas) => {
        if(pas + dataArduino.consigneAir < 40){
            dataArduino.consigneAir += pas;
        }
    },

    decreaseConsigneAir: (pas) => {
        if(pas - dataArduino.consigneAir > 0){
            dataArduino.consigneAir -= pas;
        }
    },

    setTauxHumidite : (tauxHumidite) => {
        if(!isNaN(tauxHumidite)){
            if(tauxHumidite > 80 && tauxHumidite < 100){
                dataArduino.tauxHumidite = tauxHumidite;
            }
        }
    },

    setTauxCo2: (val) => {
        dataArduino.tauxCo2 = val;
    },

    setEtatVanneFroid: (dureeAction) => {
        dataArduino.etatVanneFroid += dureeAction;

        if(dataArduino.etatVanneFroid < 0){
            dataArduino.etatVanneFroid = 0;
        }else if(dataArduino.etatVanneFroid > 40){
            dataArduino.etatVanneFroid = 40;
        }
    },

    setMoySec(temp){
        if(!isNaN(temp)){
            if(temp > 10 && temp < 40){
                dataArduino.moySec = temp;
            }
        }
    },

    setMoyHum(temp){
        if(!isNaN(temp)){
            if(temp > 10 && temp < 40){
                dataArduino.moyHum = temp;
            }
        }
    },

    getCoeff : () => {
        return dataArduino.coeff;
    },

    modifConsigneAir: (pas) => {
        let consigneAirTemp = parseFloat(dataArduino.consigneAir + pas);
        if(!isNaN(consigneAirTemp)){
            dataArduino.consigneAir = consigneAirTemp;
        }
    },

    modifConsigneHum: (pas) => {
        let consigneHumTemp = parseFloat(dataArduino.consigneHum + pas);
        if(!isNaN(consigneHumTemp)){
            dataArduino.consigneHum = consigneHumTemp;
        }
    },

    modifConsigneCo2: (pas) => {
        let consigneCo2Temp = parseFloat(dataArduino.consigneCo2 + pas);
        if(!isNaN(consigneCo2Temp)){
            dataArduino.consigneCo2 = consigneCo2Temp;
        }
    },

    ouvrirVanneAir: (duree, isEnable = true) => {
        return arduino.actionVanneAir(pinOnVanneAir, duree, isEnable);
    },

    fermerVanneAir: (duree, isEnable = true) => {
        return arduino.actionVanneAir(pinOffVanneAir, duree, isEnable);
    },

    actionVanneAir(pin, duree, isEnable){
        if(!isEnable){
            listPin[pinOnVanneAir[1]].isEnable = listPin[pinOffVanneAir].isEnable = isEnable;
        }

        let int = setInterval(() => {
            if(pin == pinOnVanneAir)
                arduino.setEtatVanneFroid(1);
            else if(pin == pinOffVanneAir){
                arduino.setEtatVanneFroid(-1);
            }
        }, 1000);

        setTimeout(() => {
            clearInterval(int);
        }, (duree + 1) * 1000);

        return useVanne(pin, duree).then(() =>{
            if(!isEnable){
                listPin[pinOnVanneAir[1]].isEnable = listPin[pinOffVanneAir].isEnable = !isEnable;
            }
        });
    },

    activerDesHum: (duree, isEnable = true) => {
        if(!isEnable){
            listPin[pinDesHum].isEnable = false;
        }

        return useVanne(pinDesHum, duree).then(() =>{
            if(!isEnable){
                listPin[pinDesHum].isEnable = true;
            }
        });
    },

    activerBrume: (duree, isEnable = true) => {
        if(!isEnable){
            listPin[pinBrume].isEnable = false;
        }

        return useVanne(pinBrume, duree).then(() =>{
            if(!isEnable){
                listPin[pinBrume].isEnable = true;
            }
        });
    },

    secureMesureAir(val){
        return new Promise((resolve, reject) => {
            tabSecuriteAir.push(val);
            if(tabSecuriteAir.length >= 5){
                let test = tabSecuriteAir[0];
                let testExeco = true;
                tabSecuriteAir.forEach(element => {
                    if(test !== element){
                        testExeco = false;
                    }
                });

                while(tabSecuriteAir.length > 4){
                    tabSecuriteAir.pop();
                }
                resolve(testExeco);
            }else{
                tabSecuriteAir.push(val);
                resolve(false);
            }
        }).then((val) => {
            if(val){
                shutDown();
            }
        });
    },

    suiviDelayGestionHum(){
        timeInterval.getGestionHumidite().then((val) => {
            let lastGestionHumidite = Date.parse(val);

            if(hToMs(1) - (Date.now() - lastGestionHumidite) < 0){
                timeBeforeGestionHum = 0;
            }else{
                timeBeforeGestionHum = hToMs(1) - (Date.now() - lastGestionHumidite);
                timeBeforeGestionHum /= 1000;
                timeBeforeGestionHum /= 60;
                timeBeforeGestionHum = arrondi(timeBeforeGestionHum);
                timeBeforeGestionHum = Math.floor(timeBeforeGestionHum);
                let delayIntHum = setInterval(() => {
                    timeBeforeGestionHum--;
                    if(timeBeforeGestionHum == 0){
                        clearInterval(delayIntHum);
                    }
                }, 60000);
            }
        })
    }
}

let board = new five.Board({port: getNamePort(), repl: false});

let timeOn = 0;
let timeOff = 0;

setTimeout(() =>{
    valVariateur = 0;
    if(board)
        listPin[pinTest].write(valVariateur);

    setInterval(() => {
        valVariateur = 130;
        if(board)
            listPin[pinTest].write(valVariateur);

        timeOn = 30;
        let delayOn = setInterval(() => {
            client.addElemClient(timeOn + " sec", "timeVarOn");
            timeOn--;
            if(timeOn == 0){
                client.addElemClient(0 + " sec", "timeVarOn");
                clearInterval(delayOn);
            }
        }, 1000);

        setTimeout(() => {
            valVariateur = 0;
            if(board)
                listPin[pinTest].write(valVariateur);

            timeOff = dataArduino.dureeVariateurOFF;
            let delayOff = setInterval(() => {
                client.addElemClient(timeOff + " sec", "timeVarOff");
                timeOff--;
                if(timeOff == 0){
                    client.addElemClient(0 + " sec", "timeVarOff");
                    clearInterval(delayOff);
                }
            }, 1000);
        }, 30000);

    }, 30000 + (dataArduino.dureeVariateurOFF) * 1000);
}, 2000)

arduino.suiviDelayGestionHum();

async function launchCron(){
    return new Promise(async (resolve, reject) => {
        updateTemperatureAir()
        .then(() => { updateTemperatureSec()}, (err) => {console.log(err);})
        .then(() => { updateTemperatureHum()}, (err) => {console.log(err);})
        .catch((err) => {console.log(err);});

        await timeInterval.getEvolConsigneInterval().then(async (val) => {
            let evolConsigne = Date.parse(val);

            if(Date.now() - evolConsigne > hToMs(1)){
                let consigne = 0;
                let objectif = 0;

                if(dataArduino.consigneAirActif){
                    consigne = dataArduino.consigneAir;
                    objectif = dataArduino.objConsigneAir;

                    if(consigne > objectif){
                        logger.info("Consigne Air > Objectif Air", {consigne:consigne, objectif:objectif});
                        consigne -= (dataArduino.modifConsigneAir / 12);
                        if(consigne <= objectif || consigne < 10){
                            if(consigne < 10){
                                logger.info("Correctif Consigne trop basse", {consigne:consigne, objectif:objectif, newConsigne:10});
                                consigne = 10;
                            }else{
                                logger.info("Objectif Air atteint", {consigne:consigne, objectif:objectif});
                            }
                            dataArduino.consigneAirActif = false;
                        }
                    }else{
                        logger.info("Consigne Air < Objectif Air", {consigne:consigne, objectif:objectif});
                        consigne += (dataArduino.modifConsigneAir / 12);
                        if(consigne >= objectif || consigne > 40){
                            if(consigne > 40){
                                logger.info("Correctif Consigne trop haute", {consigne:consigne, objectif:objectif, newConsigne:10});
                                consigne = await arduino.getTemperatureAir(false);
                            }else{
                                logger.info("Objectif Air atteint", {consigne:consigne, objectif:objectif});
                            }
                            dataArduino.consigneAirActif = false;
                        }
                    }

                    dataArduino.consigneAir = consigne;
                }

                if(dataArduino.consigneHumActif){
                    consigne = dataArduino.consigneHum;
                    objectif = dataArduino.objConsigneHum;

                    if(consigne > objectif){
                        consigne -= (dataArduino.modifConsigneHum / 12);
                        if(consigne <= objectif || consigne < 80){
                            if(consigne < 80){
                                consigne = 80;
                            }
                            dataArduino.consigneHumActif = false;
                        }
                    }else{
                        consigne += (dataArduino.modifConsigneHum / 12);
                        if(consigne >= objectif || consigne > 130){
                            if(consigne > 130){
                                consigne = 130;
                            }
                            dataArduino.consigneHumActif = false;
                        }
                    }

                    dataArduino.consigneHum = consigne;
                }

                if(dataArduino.consigneCo2Actif){
                    consigne = dataArduino.consigneCo2;
                    objectif = dataArduino.objConsigneCo2;

                    if(consigne > objectif){
                        consigne -= (dataArduino.modifConsigneCo2 / 12);
                        if(consigne <= objectif || consigne < 0){
                            if(consigne < 0){
                                consigne = 0;
                            }
                            dataArduino.consigneCo2Actif = false;
                        }
                    }else{
                        consigne += (dataArduino.modifConsigneCo2 / 12);
                        if(consigne >= objectif || consigne > 8000){
                            if(consigne > 8000){
                                consigne = 2500;
                            }
                            dataArduino.consigneCo2Actif = false;
                        }
                    }

                    dataArduino.consigneCo2 = consigne;
                }

                timeInterval.initEvolConsigne();
            }

            return 0;
        }).catch((err) => {
            // nada
            console.log(err);
        })

        await timeInterval.getEvolNbJourInterval().then((val) => {
            let evolNbJour = Date.parse(val);

            if(Date.now() - evolNbJour > hToMs(24)){
                dataArduino.nbJour++;
                let gestionCycle = require('./../../bdd/cycle');
                gestionCycle.selectLastCycle()
                .then((valeur) => {
                    gestionCycle.insertCycle({nbCycle: valeur.nbCycle, nbJour: dataArduino.nbJour});
                    })

                timeInterval.initChangeJour();
            }

            return 0;
        }).catch((err) => {
            // nada
            console.log(err);
        });

        await timeInterval.getEvolInsertDataInterval().then((val) => {
            let evolInsertData = Date.parse(val);

            if(Date.now() - evolInsertData > 300000){
                dataArduino.date = new Date();
                dataArduino._id = dataArduino.date.getTime();
                mysqlDataArduino.insert(JSON.stringify(dataArduino));
                timeInterval.initInsertData();

                gestionAir.saveMesure(arduino).catch((listErreur) => {
                    if(listErreur.code == 3){
                        arduino.getConsigneAir().then((val) => {
                            arduino.objConsigneAir = val;
                        })
                    }
                });
            }

            return 0;
        }).catch((err) => {
            // nada
            console.log(err);
        });

        resolve();
    }).then(() => {
        setTimeout(() => {
            launchCron();
        }, 30000);
        return true;
    });
}

board.on("ready", () => {
    console.log("Carte Arduino Connecte");

    initPin();

    mysqlDataArduino.selectLast()
    .then((val) => {
        val = JSON.parse(val);
        dataArduino = {...dataArduino, ...val};
        // recup valeur nouvelle bdd
        gestionAir.selectLastByQuantity(1)
        .then((lastGestionAir) => {
            lastGestionAir = lastGestionAir[0];
            dataArduino.temperatureAir = lastGestionAir.temperatureAir;
            dataArduino.consigneAir = lastGestionAir.consigneAir;
            dataArduino.objConsigneAir = lastGestionAir.objAir;
            dataArduino.modifConsigneAir = lastGestionAir.pasAir;
            dataArduino.consigneAirActif = lastGestionAir.varConsigneAirActif;
        });
        

    })
    .catch((info) => {
        console.log(`Code 001: recuperation data arduino enregistrer echouer (${info})`);
    })
    .finally(() => {

    })

            // Update mesure temperature

            
    setTimeout(() => {
        launchCron();
        gestionChampignon.launchGestion();
    }, 5000)
});


function calculHumidite(moySec, moyHum){
    let pressSaturanteSec = calculPression(moySec);
    let pressSaturanteHum = calculPression(moyHum);

    let pw = pressSaturanteHum - 1013 * 0.000662 * (moySec - moyHum);

    return pw/pressSaturanteSec * 100;
}

function calculPression(temp){
    let pression = 0;

    let tabPressionSaturante = [12.28,12.364,12.448,12.532,12.616,12.7,12.784,12.868,12.952,13.036,13.12,13.21,13.3,13.39,13.48,13.57,13.66,13.75,13.84,13.93,14.02,14.115,14.21,14.305,14.4,14.495,14.59,14.685,14.78,14.875,14.97,15.071,15.172,15.273,15.374,15.475,15.576,15.677,15.778,15.879,
        15.98,16.087,16.194,16.301,16.408,16.515,16.622,16.729,16.836,16.943,17.05,17.163,17.276,17.389,17.502,17.615,17.728,17.841,17.954,18.067,18.18,18.299,18.418,18.537,18.656,18.775,18.894,19.013,19.132,19.251,19.37,19.496,19.622,19.748,19.874,20,20.126,20.252,20.378,20.504,20.63,20.764,20.898,
        21.032,21.166,21.3,21.434,21.568,21.702,21.836,21.97,22.111,22.252,22.393,22.534,22.675,22.816,22.957,23.098,23.239,23.38,23.529,23.678,23.827,23.976,24.125,24.274,24.423,24.572,24.721,24.87,25.026,25.182,25.338,25.494,25.65,25.806,25.962,26.118,26.274,26.43,26.596,26.762,26.928,27.094,
        27.26,27.426,27.592,27.758,27.924,28.09,28.264,28.438,28.612,28.786,28.96,29.134,29.308,29.482,29.656,29.83,30.014,30.198,30.382,30.566,30.75,30.934,31.118,31.302,31.486,31.67,31.863,32.056,32.249,32.442,32.635,32.828,33.021,33.214,33.407,33.6,33.804,34.008,34.212,34.416,34.62,34.824,35.028,
        35.232,35.436,35.64,35.856,36.072,36.288,36.504,36.72,36.936,37.152,37.368,37.584,37.8,38.025,38.25,38.475,38.7,38.925,39.15,39.375,39.6,39.825,40.05,40.288,40.526,40.764,41.002,41.24,41.478,41.716,41.954,42.192,42.43,42.679,42.928,43.177,43.426,43.675,43.924,44.173,44.422,44.671,44.92,
        45.183,45.446,45.709,45.972,46.235,46.498,46.761,47.024,47.287,47.55,47.825,48.1,48.375,48.65,48.925,49.2,49.475,49.75,50.025,50.3,50.589,50.878,51.167,51.456,51.745,52.034,52.323,52.612,52.901,53.19,53.494,53.798,54.102,54.406,54.71,55.014,55.318,55.622,55.926,56.23];

    let tempIterateur = 10;
    let finTempIterateur = 35;
    let iterateur = 0;

    while(tempIterateur <= finTempIterateur && pression == 0){
        if(temp > tempIterateur - 0.05 && temp <= tempIterateur + 0.05){
            pression = tabPressionSaturante[iterateur];
        }else{
            iterateur++;
            tempIterateur += 0.1;
        }
    }
    
    return pression;
}

async function initPin(){
    await allPinToInit.forEach(async pin => {
        if(Array.isArray(pin)){
            await pin.forEach(async pinArduino => {
                await instancePin(pinArduino);
            })
        }else{
            await instancePin(pin);
        }
    });

    console.log("Pin carte Configurer");

    setInterval(() => {
        listPin[pinVentilo].query((state) => {
            let stateComplet = state;
            stateComplet.isEnable = listPin[pinVentilo].isEnable;
            client.addListenerRelay(stateComplet, "pinVentilo");
        });

        listPin[pinDesHum].query((state) => {
            let stateComplet = state;
            stateComplet.isEnable = listPin[pinDesHum].isEnable;
            client.addListenerRelay(stateComplet, "pinDesHum");
        });

        listPin[pinBrume].query((state) => {
            let stateComplet = state;
            stateComplet.isEnable = listPin[pinBrume].isEnable;
            client.addListenerRelay(stateComplet, "pinBrume");
        });

        listPin[pinOnVanneAir[1]].query((state) => {
            let stateComplet = state;
            stateComplet.isEnable = listPin[pinOnVanneAir[1]].isEnable;
            client.addListenerRelay(stateComplet, "pinOnVanneAir");
        });

        listPin[pinOffVanneAir].query((state) => {
            let stateComplet = state.state;
            stateComplet.isEnable = listPin[pinOffVanneAir].isEnable;
            client.addListenerRelay(stateComplet, "pinOffVanneAir");
        });       
    }, 1000);
}

function instancePin(pin){
    return new Promise((resolve, reject) => {
        if(!listPin[pin] && pin != 7){
            listPin[pin] = new five.Pin(pin);
            listPin[pin].isEnable = true;
            turnHigh(pin);
        }

        if(pin == 7){
            listPin[pin] = new five.Led(7);
        }
        resolve();
    })

}

function useVanne(listVanne, duree = null){
    return new Promise(async (resolve, reject) => {
        turnLow(listVanne);
        if(duree){
            setTimeout(() => {
                turnHigh(listVanne);
                resolve();
            },duree * 1000)
        }else{        
            resolve()
        }

    });
}

function turnHigh(pins){
    if(Array.isArray(pins)){
        pins.forEach(pin => {
            listPin[pin].high();
        });
    }else{
        listPin[pins].high();
    }
}

function turnLow(pins){
    if(Array.isArray(pins)){
        pins.forEach(pin => {
            listPin[pin].low();
        });
    }else{
        listPin[pins].low();
    }
}

function switchPin(pin){
    listPin[pin].query((state) =>{
        if(state.state == 1){
            turnLow(pin);
        }else{
            turnHigh(pin);
        }
    });
}

function updateTemperatureAir(){
    return new Promise((resolve, reject) => {
        getValAnalogique(pinTempAir)
        .then((state) => {
            gestionListTemp(AIR, state.value);
            let moy = traitementMoyTemp(AIR);
            if(moy){
                dataArduino.temperatureAir = moy;
            }
            resolve();
        })
        .catch((info) => {
            console.log(`Probleme lors de la recuperation valeur analogique ${AIR}`);
            console.log(info);
            shutDown();
        });
    });
}

function updateTemperatureSec(){
    return new Promise((resolve, reject) => {
        getValAnalogique(pinTempSec)
        .then((state) => {
            gestionListTemp(SEC, state.value);
            let moy = traitementMoyTemp(SEC);
            if(moy){
                dataArduino.temperatureSec = moy;
            }
            resolve();
        })
        .catch((info) => {
            console.log(`Probleme lors de la recuperation valeur analogique ${SEC}`);
            shutDown();
        });
    });
}

function updateTemperatureHum(){
    return new Promise((resolve, reject) => {
        getValAnalogique(pinTempHum)
        .then((state) => {
            gestionListTemp(HUM, state.value);
            let moy = traitementMoyTemp(HUM);
            if(moy){
                dataArduino.temperatureHum = moy;
            }
            resolve();
        })
        .catch((info) => {
            console.log(`Probleme lors de la recuperation valeur analogique ${HUM}`);
            shutDown();
        });
    });
}

// Selectionne le port de la carte arduino
function getNamePort(){
    return process.platform == "win32" ? "COM6" : "/dev/ttyUSB0";
}

// test temperature correcte
function getTemperature(temp, etal, label){
    return new Promise((resolve, reject) => {
        let temperatureFinal = temp + etal;

        if(temperatureFinal > 10 && temperatureFinal < 40 && listTemp[label].length > 10){
            resolve(temperatureFinal);
        }else{
            reject(`valeur temperature ${label} incorrecte`);
        }
    });
}

async function getTemperatureByLabel(temp, etal, label){
    await new Promise((resolve, reject) => {
        let inter = setInterval(() => {
            if(listTemp[label].length > 10){
                setTimeout(() => {
                    clearInterval(inter);
                    resolve();
                }, 1000);
            }
        }, 1000);
    });

    return await getTemperature(temp, etal, label)
    .catch((info) => {
        console.log(`Probleme recuperation temperature avec etallonage ${label}`);
        console.log(info);
    });
}

function getValAnalogique(pin){
    return new Promise((resolve, reject) => {
        let limitMesure = null;
        try{
            limitMesure = setTimeout(() => {
                reject(`Valeur Analogique indisponible pour le pin : ${pin}`);
            }, 5000);

            listPin[pin].query((state) => {
                clearTimeout(limitMesure);
                resolve(state);
            })
        }catch(err){
            reject(err);
        }
    })
}

function gestionListTemp(label, val){
    let mesure = five.Fn.map(val, 205, 1023, 100, 400) / 10;

    if(mesure > 10 && mesure < 40){
        listTemp[label].push(mesure);
        while(listTemp[label].length > 20){
            listTemp[label].shift();
        }
    }else{
        shutDown();
    }

}

function traitementMoyTemp(label){
    if(listTemp[label].length > 10){
        let listMesure = [];
        for (let i = 0; i < listTemp[label].length; i++) {
            listMesure[i] = listTemp[label][i];          
        }
        listMesure.sort((a, b) => a - b);
        listMesure = listMesure.slice(5, listMesure.length - 5);
 
        return calMoy(listMesure);
    }

    return false;
}

function shutDown(){
    process.exit(0);
}


client.addHandle("consigneAirDown" , (pas) => {
    arduino.modifConsigneAir(pas);
});

client.addHandle("consigneAirUp" , (pas) => {
    arduino.modifConsigneAir(pas);
});

client.addHandle("consigneHumDown" , (pas) => {
    arduino.modifConsigneHum(pas);
});

client.addHandle("consigneHumUp" , (pas) => {
    arduino.modifConsigneHum(pas);
});

client.addHandle("consigneCo2Down" , (pas) => {
    arduino.modifConsigneCo2(pas);
});

client.addHandle("consigneCo2Up" , (pas) => {
    arduino.modifConsigneCo2(pas);
});

client.addHandle("etalAirDown" , (pas) => {
    dataArduino.etalonageAir += pas;
});

client.addHandle("etalAirUp" , (pas) => {
    dataArduino.etalonageAir += pas;
});

client.addHandle("etalSecDown" , (pas) => {
    dataArduino.etalonageSec += pas;
});

client.addHandle("etalSecUp" , (pas) => {
    dataArduino.etalonageSec += pas;
});

client.addHandle("etalHumDown" , (pas) => {
    dataArduino.etalonageHum += pas;
});

client.addHandle("etalHumUp" , (pas) => {
    dataArduino.etalonageHum += pas;
});

client.addHandle("switchPinVentilo", () => {
    switchPin(pinVentilo);
});

client.addHandle("switchPinDesHum", () => {
    switchPin(pinDesHum);

    setTimeout(() => {
        listPin[pinDesHum].query((state) => {
            if(state.state == 0 && listPin[pinDesHum].isEnable){
                switchPin(pinDesHum);
            }
        });
    }, 600000);
});

client.addHandle("switchPinBrume", () => {
    switchPin(pinBrume);

    setTimeout(() => {
        listPin[pinBrume].query((state) => {
            if(state.state == 0 && listPin[pinBrume].isEnable){
                switchPin(pinBrume);
            }
        });
    }, 120000);
});

client.addHandle("ouvrirVanneAir", () => {
    arduino.ouvrirVanneAir(5, false);
});

client.addHandle("fermerVanneAir", () => {
    arduino.fermerVanneAir(5, false);
});

client.addHandle("fullOuvrirVanneAir", () => {
    arduino.ouvrirVanneAir(40, false);
});

client.addHandle("fullFermerVanneAir", () => {
    arduino.fermerVanneAir(40, false);
});

client.addHandle("newObjConsigneAir", (newConsigneAir) => {
    dataArduino.objConsigneAir = newConsigneAir;
    if(newConsigneAir == dataArduino.consigneAir){
        dataArduino.consigneAirActif = false;
    }else{
        dataArduino.consigneAirActif = true;
    }
});

client.addHandle("pasConsigneAir", (pasConsigneAir) => {
    dataArduino.modifConsigneAir = pasConsigneAir;
});

client.addHandle("directConsigneAir", (newConsigneAir) => {
    dataArduino.consigneAir = newConsigneAir;
});

client.addHandle("newObjConsigneHum", (newConsigneHum) => {
    dataArduino.objConsigneHum = newConsigneHum;
    if(newConsigneHum == dataArduino.consigneHum){
        dataArduino.consigneHumActif = false;
    }else{
        dataArduino.consigneHumActif = true;
    }
});

client.addHandle("newObjConsigneCo2", (newConsigneCo2) => {
    dataArduino.objConsigneCo2 = newConsigneCo2;
    if(newConsigneCo2 == dataArduino.consigneCo2){
        dataArduino.consigneCo2Actif = false;
    }else{
        dataArduino.consigneCo2Actif = true;
    }
});

client.addHandle("pasConsigneHum", (pasConsigneHum) => {
    dataArduino.modifConsigneHum = pasConsigneHum;
});


client.addHandle("pasConsigneCo2", (pasConsigneCo2) => {
    dataArduino.modifConsigneCo2 = pasConsigneCo2;
});

client.addHandle("directConsigneHum", (newConsigneHum) => {
    dataArduino.consigneHum = newConsigneHum;
});

client.addHandle("directConsigneCo2", (newConsigneCo2) => {
    dataArduino.consigneCo2 = newConsigneCo2;
});

client.addHandle("dataSetsAir", async () => {
    client.sendDataSetsCourbes(await gestionAir.selectLastByQuantity(), "actionAir");
});

client.addHandle("dataSetsHum", async () => {
    client.sendDataSetsCourbes(await gestionHum.selectLastByQuantity(), "actionHum");
})

client.addHandle("remiseAZero", async () => {
    let gestionCycle = require('./../../bdd/cycle');
    gestionCycle.selectLastCycle()
        .then((valeur) => {
            gestionCycle.insertCycle({nbCycle: valeur.nbCycle + 1, nbJour: 1});
            dataArduino.nbJour = 1;
        })

    arduino.fermerVanneAir(40, false);

    arduino.setConsigneAir(25);
    arduino.setObjConsigneAir(25);
    dataArduino.modifConsigneAir = 0;
    dataArduino.consigneAirActif = false;
    arduino.setTauxHumidite(90);
    arduino.setConsigneHum(90);
    arduino.setObjConsigneHum(90);
    dataArduino.modifConsigneHum = 0;
    dataArduino.consigneHumActif = false;
    dataArduino.tauxCo2 = 0;
    dataArduino.consigneCo2 = 2500;
    dataArduino.objConsigneCo2 = 2500;
    dataArduino.modifConsigneCo2 = 0;
    dataArduino.consigneCo2Actif = false;
});

client.addHandle("log", () =>{
    client.sendLog();
})

module.exports = {arduino:arduino};