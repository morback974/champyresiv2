const {delay} = require('../util/utilitaire');

let pendingRequest = [];
let etat = 0;

function getCo2(room){
    return new Promise((resolve, reject) => {
        pendingRequest.push({room:room, resolve:resolve});
        if(etat == 0){
            etat = 1;
            launchProcessCO2();
        }
    })
}

async function launchProcessCO2(){
    let request = pendingRequest[0];

    let co2 = 0;
    let co2Room = 0;

    try{
        console.log("Pompe air salle champ: ", request.room);
        activationRelay(request.room, 102);
        await delay(1, "seconde");
        activationRelay(38, 100);
        co2Room = await mesureCO2(90);
        await delay(15, 'seconde');
    
        console.log("Pompe air ext")
        activationRelay(7, 25);
        await delay(1, "seconde");
        activationRelay(38, 20);
        co2 = await mesureCO2(10);
        await delay(15, "seconde");
    
        console.log("Envoie valeur : ", co2Room - 500);
        console.log("Fin demande co2: ", request.room);
        if(co2 > 0 && co2 < 4000){
            co2Room -= 500;
            request.resolve(co2Room);
            pendingRequest.shift();
    
            if(pendingRequest.length > 0){
                launchProcessCO2();
            }else{
                etat = 0;
            }
        }
    }catch(err){
        console.log(err, "Erreur lors de la mesure du co2");
        request.reject();
        pendingRequest.shift();

        if(pendingRequest.length > 0){
            launchProcessCO2();
        }else{
            etat = 0;
        }
    }
}

function getPin(pin){
    let relayPinGPIO = 0;

    switch(parseInt(pin)){
        case 1: relayPinGPIO = 32;
            break;

        case 2: relayPinGPIO = 22;
            break;

        case 3: relayPinGPIO = 16;
            break;

        case 4: relayPinGPIO = 18;
            break;

        case 5: relayPinGPIO = 29;
            break;

        case 6: relayPinGPIO = 31;
            break;

        case 7: relayPinGPIO = 37;
            break;

        default: relayPinGPIO = pin;
            break;
    }

    return relayPinGPIO;
}

function activationRelay(relay, duree = 100){
    let relayPinGPIO = getPin(relay);
    console.log("Debut activation relay (" + relay + ")");

    const {spawn} = require('child_process');

    const python = spawn("python", ['relayActivation.py', relayPinGPIO, duree]);
    python.stdout.on('data', (data) => {
        console.log("Fin activation relay (" + relayPinGPIO + ")");
    });

    python.on('close', (data) => {
        console.log("Affichage vrai fin Script");
    });
}

function mesureCO2(nbMesure = 90){
    return new Promise((resolve, reject) => {
        const {spawn} = require('child_process');
        let python = null;

        if(process.platform == "win32"){
            python = spawn("python", ['../test/testScript.py', nbMesure]);
        }else{
            python = spawn("python", ['K-30.py', nbMesure]);
        }

        python.stdout.on('data', (data) => {
            resolve(data.toString());
        })
    })
}

module.exports = {getCo2: getCo2}