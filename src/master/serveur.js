const express = require('express');
const appCO2 = express();

const CO2_server = appCO2.listen(3100, () => {
    console.log('Serveur Master Co2 lancer (port 3100)');
});

require('./../router/CO2')(appCO2, express);