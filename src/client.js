let io = null;

let infoChampignon = {};
let infoRelay = {};
let listHandle = [];
let log = [];
let listClient = new Map();

function updateSendClient(){
    setInterval(() => {
        if(infoChampignon){
            send(infoChampignon);
        }

        if(infoRelay){
            send(infoRelay, "infoRelay");
        }
    }, 1000);
}

let logCompteur = [];

function send(message, label = "message"){
    if(io){
        if(!logCompteur[label]){
            logCompteur[label] = 1;
        }else{
            logCompteur[label]++;
        }

        //logCompteur[label + "BIS"] = message;
        //io.emit("logCompteur", logCompteur);
        io.emit(label, message);
    }
}

function sendTo(id, data, label = "message"){
    listClient.get(id).emit(label, data);
}

module.exports = {
    send:send,
    sendTo:sendTo,
    config: (server) => {
        return new Promise((resolve, reject) => {
            if(!io){
                io = require('socket.io')(server, {cookie:false});
                io.on("connection", (socket) => {
                    listClient.set(socket.id, socket);
                    listHandle.forEach(element => {
                        socket.on(element[0], element[1]);
                    });

                    socket.on("disconnect", () => {
                        listClient.delete(socket);                       
                    })
                })
                updateSendClient();
            }

            resolve();
        })
    },

    addElemClient: (valeur, selecteur = null, isClass = true) => {
        if(selecteur){
            infoChampignon[selecteur] = {valeur:valeur, isClass:isClass, selecteur:selecteur};
        }else{
            valeur.forEach((elem) => {
                infoChampignon[elem[1]] = {valeur:elem[0], isClass:isClass, selecteur:elem[1]};
            });
        }
    },

    sendDataSetsCourbes: async (fullDataSets, info) => {
        let dataSets = null;
        if(info == "actionAir"){
            dataSets = await processActionAir(fullDataSets);
            send(JSON.stringify({labels:dataSets["labels"], temperatureAir:dataSets["temperatureAir"], consigneAir:dataSets["consigneAir"], etatVanneFroidAfter:dataSets["etatVanneFroidAfter"], nbJour: dataSets["nbJour"]}), "dataSetsAir");
        }

        if(info == "actionHum"){
            dataSets = await processActionHum(fullDataSets);
            send(JSON.stringify({labels:dataSets["labels"], temperatureSec:dataSets["temperatureSec"], temperatureHum:dataSets["temperatureHum"], tauxHumidite:dataSets["tauxHumidite"], consigneHum:dataSets["consigneHum"], nbJour: dataSets["nbJour"], tempsDeshum: dataSets["tempsDeshum"], tempsBrume: dataSets["tempsBrume"]}), "dataSetsHum");
        }
    },

    addHandle: (emitter, cb) => {
        listHandle.push([emitter, cb]);
    },

    addListenerRelay: (valeur, pin) => {
        infoRelay[pin] = {valeur:valeur, pin:pin}
    },

    sendLog: (newLog = null) => {
        if(!newLog){
            send(log, "log");
        }else{
            newLog.details = JSON.stringify(newLog.details);
            log.push(newLog);
            send(log, "log");
        }
    }
}

function processActionAir(fullDataSets){
    return new Promise((resolve, reject) => {
        let orderByAsc = fullDataSets.reverse();
        let arrFiltred = [];
        arrFiltred["labels"] = [];
        arrFiltred["temperatureAir"] = [];
        arrFiltred["consigneAir"] = [];
        arrFiltred["etatVanneFroidAfter"] = [];
        arrFiltred["nbJour"] = [];

        let etatVanneFroidAfter = 0;

        for(let i = 0; i < orderByAsc.length; i++){
            arrFiltred["labels"][i] = orderByAsc[i].mesuredAt;
            arrFiltred["temperatureAir"][i] = orderByAsc[i].temperatureAir;
            arrFiltred["consigneAir"][i] = orderByAsc[i].consigneAir;
            arrFiltred["nbJour"][i] = orderByAsc[i].nbJour;

            if(orderByAsc[i].etatVanneFroidAfter || orderByAsc[i].etatVanneFroidAfter == 0){
                arrFiltred["etatVanneFroidAfter"][i] = orderByAsc[i].etatVanneFroidAfter;
                etatVanneFroidAfter = orderByAsc[i].etatVanneFroidAfter;
            }else{
                arrFiltred["etatVanneFroidAfter"][i] = etatVanneFroidAfter;
            }
        }
        resolve(arrFiltred);
    });
}

function processActionHum(fullDataSets){
    return new Promise((resolve, reject) => {
        let orderbyAsc = fullDataSets.reverse();
        let arrFiltred = [];
        arrFiltred["labels"] = [];
        arrFiltred["temperatureSec"] = [];
        arrFiltred["temperatureHum"] = [];
        arrFiltred["tauxHumidite"] = [];
        arrFiltred["consigneHum"] = [];
        arrFiltred["nbJour"] = [];
        arrFiltred["tempsDeshum"] = [];
        arrFiltred["tempsBrume"] = [];

        for(let i = 0; i < orderbyAsc.length; i++){
            arrFiltred["labels"][i] = orderbyAsc[i].mesuredAt;
            arrFiltred["temperatureSec"][i] = orderbyAsc[i].temperatureSec;
            arrFiltred["temperatureHum"][i] = orderbyAsc[i].temperatureHum;
            arrFiltred["tauxHumidite"][i] = orderbyAsc[i].tauxHumidite;
            arrFiltred["consigneHum"][i] = orderbyAsc[i].consigneHum;
            arrFiltred["nbJour"][i] = orderbyAsc[i].nbJour;
            arrFiltred["tempsDeshum"][i] = orderbyAsc[i].tempsDeshum;
            arrFiltred["tempsBrume"][i] = orderbyAsc[i].tempsBrume;
        }

        resolve(arrFiltred);
    })
}