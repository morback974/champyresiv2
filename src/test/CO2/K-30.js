let serialPort = require('serialport');
const Ready = require('@serialport/parser-ready')
let conSerial = new serialPort("/dev/ttyUSB0");


const parser = port.pipe(new Ready({ delimiter: 'READY' }))
parser.on('ready', () => console.log('the ready byte sequence has been received'))
parser.on('data', console.log) // all data after READY is received

conSerial.flush(() => {
    setTimeout(async () => {
        for (let i = 0; i < 21; i++) {
            await new Promise((resolve, reject) => {
                conSerial.flush();
                conSerial.write("\xFE\x44\x00\x08\x02\x9F\x25", 
                () => {
                    setTimeout(() => {
                        let resp = conSerial.read(7);
    
                        let high = resp.charCodeAt(3);
                        let low = resp.charCodeAt(4);
                        let co2 = (high*256) + low;
                        console.log(`i = ${i} CO2 = ${co2.toString()}`);
                        resolve();
                    }, 500);
                }
                );
            });
        }
    }, 1000);
});
