#!/usr/bin/env python
# Test du port srie
import serial
test_string = "Je teste le port srie 1 2 3 4 5".encode('utf-8')
port_list = ["/dev/ttyAMA0","/dev/ttyAMA0","/dev/ttyS0","/dev/ttyS"]
for port in port_list:
  try:
    serialPort = serial.Serial(port, 9600, timeout = 2)
    print ("Port Srie", port, " ouvert pour le test:")
    bytes_sent = serialPort.write(test_string)
    print ("Envoy", bytes_sent, "octets")
    loopback = serialPort.read(bytes_sent)
    if loopback == test_string:
      print ("Reu ",len(loopback), "octets identiques. Le port", port,"fonctionne bien ! \n")
    else:
      print ("Reu des donnes incorrectes:", loopback, "sur le port srie", port, "boucl \n")
    serialPort.close()
  except IOError:
    print ("Erreur sur", port,"\n")