import RPi.GPIO as GPIO
import time

GPIO.setmode(GPIO.BOARD)

GPIO.setup(32, GPIO.OUT)
GPIO.output(32, GPIO.HIGH)
#etat eteint

GPIO.setup(36, GPIO.OUT)
GPIO.output(36, GPIO.LOW)
# allumer eteint

time.sleep(5)
GPIO.cleanup()