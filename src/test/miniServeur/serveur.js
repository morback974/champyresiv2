const express = require('express');
const app = express();

const server = app.listen(3000,() => {
    console.log('Serveur Web lancer (port 3000)');
});

let io = null;
io = require('socket.io')(server, {cookie:false});

let nbTest = 0;
setTimeout(() => {
    setInterval(() => {
        nbTest++;
        io.emit("testRouteur", nbTest);
    }, 1000);
}, 5000);

const path = require('path');
const cons = require('consolidate');


const router = express.Router();

app.engine('html', cons.swig)
app.set('public', path.join(__dirname, '../../../../public'));
app.set('view engine', 'html');
app.use(
    express.static(__dirname + '../../../../public')
);
app.use('/', router);

router.get('/', (req, res) => {
    res.type('html');
    let renderedViews = "";

    app.render(path.join(__dirname + '../../../../public', 'header'), 
        (err, html) => {
        renderedViews += html;
        app.render(path.join(__dirname + '../../../../src/test/miniServeur', 'home'), 
            (err, html) => {
                renderedViews += html;
                res.send(renderedViews);
            }
        );
        }
    );
});