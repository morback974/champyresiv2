$(document).ready(() => {
    $('#etalAirDown').click(() => {
        socket.emit("etalAirDown", -0.1);
    });

    $('#etalAirUp').click(() => {
        socket.emit("etalAirUp", 0.1);
    });

    $('#etalSecDown').click(() => {
        socket.emit("etalSecDown", -0.1);
    });

    $('#etalSecUp').click(() => {
        socket.emit("etalSecUp", 0.1);
    });

    $('#etalHumDown').click(() => {
        socket.emit("etalHumDown", -0.1);
    });

    $('#etalHumUp').click(() => {
        socket.emit("etalHumUp", 0.1);
    });
});