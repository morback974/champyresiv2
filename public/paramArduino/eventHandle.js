$(document).ready(() => {
    $(".btnObjConsigneAir").click(() => {
        if($("#newObjConsigneAir").val() > 10 && $("#newObjConsigneAir").val() < 40){
            socket.emit("newObjConsigneAir", $("#newObjConsigneAir").val());
        }
    });

    $(".btnPasConsigneAir").click(() => {
        if(!isNaN($("#newPasConsigneAir").val()) && $("#newPasConsigneAir").val() >= 0){
            socket.emit("pasConsigneAir", $("#newPasConsigneAir").val());
        }
    });

    $(".directConsigneAir").click(() => {
        if($("#saisieDirectConsigneAir").val() > 10 && $("#saisieDirectConsigneAir").val() < 40){
            socket.emit("directConsigneAir", $("#saisieDirectConsigneAir").val());
        }
    });

    $(".btnObjConsigneHum").click(() => {
        if($("#newObjConsigneHum").val() > 80 && $("#newObjConsigneHum").val() < 100){
            socket.emit("newObjConsigneHum", $("#newObjConsigneHum").val());
        }
    });

    $(".btnPasConsigneHum").click(() => {
        if(!isNaN($("#newPasConsigneHum").val()) && $("#newPasConsigneHum").val() >= 0){
            socket.emit("pasConsigneHum", $("#newPasConsigneHum").val());
        }
    });

    $(".directConsigneHum").click(() => {
        if($("#saisieDirectConsigneHum").val() > 80 && $("#saisieDirectConsigneHum").val() < 100){
            socket.emit("directConsigneHum", $("#saisieDirectConsigneHum").val());
        }
    });


    $(".btnObjConsigneCo2").click(() => {
        if($("#newObjConsigneCo2").val() > 0 && $("#newObjConsigneCo2").val() < 8000){
            socket.emit("newObjConsigneCo2", $("#newObjConsigneCo2").val());
        }
    });

    $(".btnPasConsigneCo2").click(() => {
        if(!isNaN($("#newPasConsigneCo2").val()) && $("#newPasConsigneCo2").val() >= 0){
            socket.emit("pasConsigneCo2", $("#newPasConsigneCo2").val());
        }
    });

    $(".directConsigneCo2").click(() => {
        if($("#saisieDirectConsigneCo2").val() > 0 && $("#saisieDirectConsigneCo2").val() < 8000){
            socket.emit("directConsigneCo2", $("#saisieDirectConsigneCo2").val());
        }
    });

    $(".remiseAZero").click(() => {
        socket.emit("remiseAZero", true);
    });

    socket.on("message", (data) => {
        gestionTempsEstime(data);
    });
});

function gestionTempsEstime(data){
    if(data["consigneAirActif"].valeur && data["pasConsigneAir"] && data["consigneAir"] && data["objConsigneAir"]){
        let pas = data["pasConsigneAir"].valeur / 12;
        let dureeHeure = Math.abs(data["objConsigneAir"].valeur - data["consigneAir"].valeur) / pas;
        let nbJourTotal = dureeHeure / 24;
        let nbJour = Math.floor(nbJourTotal);
        let heureRestant = (nbJourTotal - nbJour) * 24;

        $(".tempsRestantConsigneAir").text(nbJour + " Jour et " + Math.round(heureRestant) + " Heure");
        
        $('.statusObjAir').removeClass("bg-danger bg-warning")
        .addClass("bg-success");
        $('.statusTxtAir').text("Objectif Air Activer");
    }else{
        $('.statusObjAir').removeClass("bg-success bg-warning")
        .addClass("bg-danger");
        $('.statusTxtAir').text("Objectif Air Desactiver");
    }

    if(data["consigneHumActif"].valeur && data["pasConsigneHum"] && data["consigneHum"] && data["objConsigneHum"]){
        let pas = data["pasConsigneHum"].valeur / 12;
        let dureeHeure = Math.abs(data["objConsigneHum"].valeur - data["consigneHum"].valeur) / pas;
        let nbJourTotal = dureeHeure / 24;
        let nbJour = Math.floor(nbJourTotal);
        let heureRestant = (nbJourTotal - nbJour) * 24;

        $(".tempsRestantConsigneHum").text(nbJour + " Jour et " + Math.round(heureRestant) + " Heure");

        $('.statusObjHum').removeClass("bg-danger bg-warning")
        .addClass("bg-success");
        $('.statusTxtHum').text("Objectif Hum Activer");
    }else{
        $('.statusObjHum').removeClass("bg-success bg-warning")
        .addClass("bg-danger");
        $('.statusTxtHum').text("Objectif Hum Desactiver");
    }

    if(data["consigneCo2Actif"].valeur && data["pasConsigneCo2"] && data["consigneCo2"] && data["objConsigneCo2"]){
        let pas = data["pasConsigneCo2"].valeur / 12;
        let dureeHeure = Math.abs(data["objConsigneCo2"].valeur - data["consigneCo2"].valeur) / pas;
        let nbJourTotal = dureeHeure / 24;
        let nbJour = Math.floor(nbJourTotal);
        let heureRestant = (nbJourTotal - nbJour) * 24;

        $(".tempsRestantConsigneCo2").text(nbJour + " Jour et " + Math.round(heureRestant) + " Heure");

        $('.statusObjCo2').removeClass("bg-danger bg-warning")
        .addClass("bg-success");
        $('.statusTxtCo2').text("Objectif Co2 Activer");
    }else{
        $('.statusObjCo2').removeClass("bg-success bg-warning")
        .addClass("bg-danger");
        $('.statusTxtCo2').text("Objectif Co2 Desactiver");
    }
}