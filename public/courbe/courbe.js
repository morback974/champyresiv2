let choixCourbe = "dataSetsAir";
let timeoutCourbe = null;

$(document).ready(() => {
    socket.emit(choixCourbe);

    socket.on("dataSetsAir", (dataSets) => {
        if(choixCourbe == "dataSetsAir"){
            showCourbeAir(dataSets);

            if(timeoutCourbe)
                 clearTimeout(timeoutCourbe);
     
             timeoutCourbe = setTimeout(() => {
                 let changeCourbe = choixCourbe;
                 socket.emit(changeCourbe);
             }, 300000);
        }
    });

    socket.on("dataSetsHum", (dataSets) => {
        if(choixCourbe == "dataSetsHum"){
            console.log(dataSets);
            showCourbeHum(dataSets);

            if(timeoutCourbe)
            clearTimeout(timeoutCourbe);

            timeoutCourbe = setTimeout(() => {
                let changeCourbe = choixCourbe;
                socket.emit(changeCourbe);
            }, 300000);
        }
    });

    $('#choixCourbeSelect').on('change', function() {
        choixCourbe = this.value;
        socket.emit(choixCourbe);

        if(choixCourbe == "dataSetsHum"){
            $('.libelleCourbe').text("Courbes Humidite");
            $('.libelleEtal').text("Suivie Action Humidite");
        }else{
            $('.libelleCourbe').text("Courbes Air");
            $('.libelleEtal').text("Etat Vanne Air");
        }
    });
});

function saveCourbe(data){
    localStorage.setItem(choixCourbe, data);
}

function showCourbeHum(dataSets, save = true){
    if(save){
        saveCourbe(dataSets);
    }

    dataSets = JSON.parse(dataSets);

    let dataTauxHum = [];
    let dataConsHum = [];
    let dataTempsDeshum = [];
    let dataTempsBrume = [];
    let nbJour = [];

    dataSets.tauxHumidite.forEach((element, index) => {
        dataTauxHum.push({x: new moment(dataSets.labels[index]), y: element});
        dataConsHum.push({x: new moment(dataSets.labels[index]), y: dataSets.consigneHum[index]});
        if(dataSets.tempsDeshum[index])
            dataTempsDeshum.push({x: new moment(dataSets.labels[index]), y: dataSets.tempsDeshum[index] != 0 ? 1 : 0});
        if(dataSets.tempsBrume[index])
            dataTempsBrume.push({x: new moment(dataSets.labels[index]), y: dataSets.tempsBrume[index] != 0 ? -1 : 0});
        nbJour.push({heure: new moment(dataSets.labels[index]), jour: dataSets.nbJour[index]});
    });

    var ctx = document.getElementById('chartGestionAir').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {  
            labels: nbJour,          
            datasets: [{
                label: 'Taux humidite',
                xAxisID:'xAxis1',
                data: dataTauxHum,
                backgroundColor: 
                    'rgba(255, 99, 132, 0.2)'
                ,
                borderColor: 
                    'rgba(255, 99, 132, 1)'
                ,
            },
            {
                label: 'Consigne Humidite',
                data: dataConsHum,
                backgroundColor: 
                    'rgba(54, 162, 235, 0.2)'
                ,
                borderColor: 
                    'rgba(54, 162, 235, 1)'
                ,
                borderWidth: 1
            }
        ]
        },
        options: {
            scales: {
                yAxes: [{
                    display: true,
                    position: 'right',
                    ticks: {
                        beginAtZero: false,
                        min: 60,
                        max: 100,
                        stepSize: 1
                    }
                },
                {
                    display: true,
                    position: 'left',
                    ticks: {
                        beginAtZero: false,
                        min: 60,
                        max: 100,
                        stepSize: 1
                    }
                }
            
            ],
                
                xAxes: [{
                    id:'xAxis1',
                    type: 'time',
                    display: false,
                    time:{
                    displayFormats: {
                        quarter: 'HH:mm'
                    }},
                    // ticks:{
                    //     callback:function(label){
                    //         return label.heure.hour() + ":" + label.heure.format("mm");
                    //     }
                    // }
                },{
                    id:'xAxis2',
                    type: 'category',
                    display: true,
                    time:{
                    displayFormats: {
                        quarter: 'HH:mm'
                    }},
                    ticks:{
                        callback:function(label){
                            return label.heure.hour() + ":" + label.heure.format("mm");
                        }
                    }
                },
                {
                    id:'xAxis3',
                    type:"category",
                    gridLines: {
                        drawOnChartArea: false, // only want the grid lines for one axis to show up
                    },
                    ticks:{
                        callback: function(label){
                            return "Jour " + label.jour;
                        }
                    }
                }
            ],
                
            },
            
            elements: {
                point:{
                    radius: 0
                }
            }
        }
    });

    var ctx = document.getElementById('chartEtal').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {     
            labels: nbJour,       
            datasets: [
            {
                label: 'Temps Deshum',
                xAxisID:'xAxis1',
                data: dataTempsDeshum,
                backgroundColor: 
                    'rgba(255, 205, 86, 0.2)'
                ,
                borderColor: 
                    'rgba(255, 205, 86, 1)'
                ,
                borderWidth: 1
            },
            {
                label: 'Temps Brume',
                data: dataTempsBrume,
                backgroundColor: 
                    'rgba(54, 162, 235, 0.2)'
                ,
                borderColor: 
                    'rgba(54, 162, 235, 1)'
                ,
                borderWidth: 1
            }
        ]
        },
        options: {
            scales: {
                yAxes: [{
                    display: true,
                    position: 'right',
                    ticks: {
                        beginAtZero: false,
                        min: -1,
                        max: 1,
                        stepSize: 1
                    }
                },
                {
                    display: true,
                    position: 'left',
                    ticks: {
                        beginAtZero: false,
                        min: -1,
                        max: 1,
                        stepSize: 1
                    }
                }
            ],
                
            xAxes: [{
                id:'xAxis1',
                type: 'time',
                display: false,
                time:{
                displayFormats: {
                    quarter: 'HH:mm'
                }},
                // ticks:{
                //     callback:function(label){
                //         return label.heure.hour() + ":" + label.heure.format("mm");
                //     }
                // }
            },{
                id:'xAxis2',
                type: 'category',
                display: true,
                time:{
                displayFormats: {
                    quarter: 'HH:mm'
                }},
                ticks:{
                    callback:function(label){
                        return label.heure.hour() + ":" + label.heure.format("mm");
                    }
                }
            },
            {
                id:'xAxis3',
                type:"category",
                gridLines: {
                    drawOnChartArea: false, // only want the grid lines for one axis to show up
                },
                ticks:{
                    callback: function(label){
                        return "Jour " + label.jour;
                    }
                }
            }
        ],
                
            },
            
            elements: {
                point:{
                    radius: 0
                }
            }
        }
    });
}

function showCourbeAir(dataSets, save = true){
    if(save){
        saveCourbe(dataSets);
    }

    dataSets = JSON.parse(dataSets);

    let dataTemp = [];
    let dataCons = [];
    let dataEtatVanne = [];
    let nbJour = [];

    dataSets.temperatureAir.forEach((element, index) => {
        dataTemp.push({x: new moment(dataSets.labels[index]), y: element});
        dataCons.push({x: new moment(dataSets.labels[index]), y: dataSets.consigneAir[index]});
        dataEtatVanne.push({x: new moment(dataSets.labels[index]), y: (dataSets.etatVanneFroidAfter[index] * 100 / 40)});
        nbJour.push({heure: new moment(dataSets.labels[index]), jour: dataSets.nbJour[index]});
    });
    
    var ctx = document.getElementById('chartGestionAir').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {  
            labels: nbJour,          
            datasets: [{
                label: 'Temperature Air',
                xAxisID:'xAxis1',
                data: dataTemp,
                backgroundColor: 
                    'rgba(255, 99, 132, 0.2)'
                ,
                borderColor: 
                    'rgba(255, 99, 132, 1)'
                ,
            },
            {
                label: 'Consigne Air',
                data: dataCons,
                backgroundColor: 
                    'rgba(54, 162, 235, 0.2)'
                ,
                borderColor: 
                    'rgba(54, 162, 235, 1)'
                ,
                borderWidth: 1
            }
        ]
        },
        options: {
            scales: {
                yAxes: [{
                    display: true,
                    position: 'right',
                    ticks: {
                        beginAtZero: false,
                        min: 10,
                        max: 30,
                        stepSize: 1
                    }
                },
                {
                    display: true,
                    position: 'left',
                    ticks: {
                        beginAtZero: false,
                        min: 10,
                        max: 30,
                        stepSize: 1
                    }
                }
            
            ],
                
                xAxes: [{
                    id:'xAxis1',
                    type: 'time',
                    display: false,
                    time:{
                    displayFormats: {
                        quarter: 'HH:mm'
                    }},
                    // ticks:{
                    //     callback:function(label){
                    //         return label.heure.hour() + ":" + label.heure.format("mm");
                    //     }
                    // }
                },{
                    id:'xAxis2',
                    type: 'category',
                    display: true,
                    time:{
                    displayFormats: {
                        quarter: 'HH:mm'
                    }},
                    ticks:{
                        callback:function(label){
                            return label.heure.hour() + ":" + label.heure.format("mm");
                        }
                    }
                },
                {
                    id:'xAxis3',
                    type:"category",
                    gridLines: {
                        drawOnChartArea: false, // only want the grid lines for one axis to show up
                    },
                    ticks:{
                        callback: function(label){
                            return "Jour " + label.jour;
                        }
                    }
                }
            ],
                
            },
            
            elements: {
                point:{
                    radius: 0
                }
            }
        }
    });

    var ctx = document.getElementById('chartEtal').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {            
            datasets: [
            {
                label: 'Etat Vanne Froid',
                data: dataEtatVanne,
                backgroundColor: 
                    'rgba(255, 205, 86, 0.2)'
                ,
                borderColor: 
                    'rgba(255, 205, 86, 1)'
                ,
                borderWidth: 1
            }
        ]
        },
        options: {
            scales: {
                yAxes: [{
                    display: true,
                    position: 'right',
                    ticks: {
                        beginAtZero: false,
                        min: 0,
                        max: 100,
                        stepSize: 1
                    }
                },
                {
                    display: true,
                    position: 'left',
                    ticks: {
                        beginAtZero: false,
                        min: 0,
                        max: 100,
                        stepSize: 1
                    }
                }
            ],
                
                xAxes: [{
                    type: 'time',
                    time:{
                    displayFormats: {
                        quarter: 'HH:mm'
                    }}
                }],
                
            },
            
            elements: {
                point:{
                    radius: 0
                }
            }
        }
    });
}