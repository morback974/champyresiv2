$(document).ready(() => {
    socket.on("log", (log) => {
        generateTableauLog(log);
    });

   socket.emit("log", true); 
})

function generateTableauLog(log){
    let htmlTabLog = "";
    log.reverse();

    log.forEach((element, index) => {
        htmlTabLog += generateLigneLog(element, log.length - index);
    });

    $(".tabLog").html(htmlTabLog);
}

function generateLigneLog(log, index){
    let dateBrut = new moment(log.date);
    let dateFormatted = dateBrut.format("hh:mm:ss DD/MM/YY")

    return `<tr>
    <th scope="row">${index}</th>
    <td>${log.niveau}</td>
    <td>${dateFormatted}</td>
    <td>${log.message}</td>
    <td>${log.details}</td>
  </tr>`;

}