let disabledBtnVanneAir = true;
let disabledBtnBrume = true;
let disabledBtnVentilo = true;
let disabledBtnDesHum = true;

$(document).ready(() => {
    $('.switchRelayVentilo').click(() => {
        socket.emit("switchPinVentilo", true);
    });

    $('.switchRelayDesHum').click(() => {
        socket.emit("switchPinDesHum", true);
    });

    $('.switchRelayBrume').click(() => {
        socket.emit("switchPinBrume", true);
    });
  
    $('.ouvrirVanneAir').click(() => {
        $(".ouvrirVanneAir").prop("disabled",true);
        $(".fermerVanneAir").prop("disabled",true);
        disabledBtnVanneAir = true;
        $('.ouvrirVanneAir').removeClass("btn-success btn-danger");
        $('.ouvrirVanneAir').addClass("btn-light");

        $('.fermerVanneAir').removeClass("btn-success btn-danger");
        $('.fermerVanneAir').addClass("btn-light");
        socket.emit("ouvrirVanneAir", true);
    });

    $('.fermerVanneAir').click(() => {
        $(".ouvrirVanneAir").prop("disabled",true);
        $(".fermerVanneAir").prop("disabled",true);
        disabledBtnVanneAir = true;
        $('.ouvrirVanneAir').removeClass("btn-success btn-danger");
        $('.ouvrirVanneAir').addClass("btn-light");

        $('.fermerVanneAir').removeClass("btn-success btn-danger");
        $('.fermerVanneAir').addClass("btn-light");
        socket.emit("fermerVanneAir", true);
    });

    $('.fullOuvrirVanneAir').click(() => {
        socket.emit("fullOuvrirVanneAir", true);
    });

    $('.fullFermerVanneAir').click(() => {
        socket.emit("fullFermerVanneAir", true);
    });

    socket.on("infoRelay", (data) => {
        hydrateRelay(data);
    });
});

function hydrateRelay(data){
    if(data["pinVentilo"]){
        gestionRelayVentilo(data["pinVentilo"]);
    }

    if(data["pinDesHum"]){
        gestionRelayDesHum(data["pinDesHum"]);
    }

    if(data["pinBrume"]){
        gestionRelayBrume(data["pinBrume"]);
    }

    if(data["pinOnVanneAir"]){;
        gestionRelayVanneAir(data["pinOnVanneAir"]);
    }
}

function gestionRelayVentilo(state){
    if(state.valeur.isEnable){
        if(disabledBtnVentilo){
            disabledBtnVentilo = false;
        }else{
            $(".switchRelayVentilo").prop("disabled",false);
        }

        if(state.valeur.state == 1){
            $('.switchRelayVentilo').removeClass("btn-light btn-danger");
            $('.switchRelayVentilo').addClass("btn-success");

            $('.switchRelayVentilo').text("Ouvrir");
            $('.etatRelayVentilo').text("Fermé");
        }else{
            $('.switchRelayVentilo').removeClass("btn-light btn-success");
            $('.switchRelayVentilo').addClass("btn-danger");

            $('.switchRelayVentilo').text("Fermer");
            $('.etatRelayVentilo').text("Ouvert");
        }
    }else{
        $(".switchRelayVentilo").prop("disabled",true);

        $('.switchRelayVentilo').removeClass("btn-danger btn-success");
        $('.switchRelayVentilo').addClass("btn-light");

        $('.etatRelayVentilo').text("En cours utilisation");

        if(state.valeur.state == 1){
            $('.switchRelayVentilo').text("Ouvrir");
        }else{
            $('.switchRelayVentilo').text("Fermer");
        }
    }
}

function gestionRelayDesHum(state){
    if(state.valeur.isEnable){
        if(disabledBtnDesHum){
            disabledBtnDesHum = false;
        }else{
            $(".switchRelayDesHum").prop("disabled",false);
        }

        if(state.valeur.state == 1){
            $('.switchRelayDesHum').removeClass("btn-light btn-danger");
            $('.switchRelayDesHum').addClass("btn-success");

            $('.switchRelayDesHum').text("Ouvrir");
            $('.etatRelayDesHum').text("Fermé");
        }else{
            $('.switchRelayDesHum').removeClass("btn-light btn-success");
            $('.switchRelayDesHum').addClass("btn-danger");

            $('.switchRelayDesHum').text("Fermer");
            $('.etatRelayDesHum').text("Ouvert");
        }
    }else{
        $(".switchRelayDesHum").prop("disabled",true);

        $('.switchRelayDesHum').removeClass("btn-danger btn-success");
        $('.switchRelayDesHum').addClass("btn-light");

        $('.etatRelayDesHum').text("En cours utilisation");

        if(state.valeur.state == 1){
            $('.switchRelayDesHum').text("Ouvrir");
        }else{
            $('.switchRelayDesHum').text("Fermer");
        }
    }
}

function gestionRelayBrume(state){
    if(state.valeur.isEnable){
        if(disabledBtnBrume){
            disabledBtnBrume = false;
        }else{
            $(".switchRelayBrume").prop("disabled",false);
        }

        if(state.valeur.state == 1){
            $('.switchRelayBrume').removeClass("btn-light btn-danger");
            $('.switchRelayBrume').addClass("btn-success");

            $('.switchRelayBrume').text("Ouvrir");
            $('.etatRelayBrume').text("Fermé");
        }else{
            $('.switchRelayBrume').removeClass("btn-light btn-success");
            $('.switchRelayBrume').addClass("btn-danger");

            $('.switchRelayBrume').text("Fermer");
            $('.etatRelayBrume').text("Ouvert");
        }
    }else{
        $(".switchRelayBrume").prop("disabled",true);

        $('.switchRelayBrume').removeClass("btn-danger btn-success");
        $('.switchRelayBrume').addClass("btn-light");

        $('.etatRelayBrume').text("En cours utilisation");

        if(state.valeur.state == 1){
            $('.switchRelayBrume').text("Ouvrir");
        }else{
            $('.switchRelayBrume').text("Fermer");
        }
    }
}

function gestionRelayVanneAir(state){
    if(state.valeur.isEnable){
        if(disabledBtnVanneAir){
            disabledBtnVanneAir = false;
        }else{
            $(".ouvrirVanneAir").prop("disabled",false);
            $(".fermerVanneAir").prop("disabled",false);
        }

        $('.ouvrirVanneAir').removeClass("btn-light btn-danger");
        $('.ouvrirVanneAir').addClass("btn-success");

        $('.fermerVanneAir').removeClass("btn-light btn-danger");
        $('.fermerVanneAir').addClass("btn-danger");
    }else if(!state.valeur.isEnable){
        $(".ouvrirVanneAir").prop("disabled",true);
        $(".fermerVanneAir").prop("disabled",true);

        $('.ouvrirVanneAir').removeClass("btn-success btn-danger");
        $('.ouvrirVanneAir').addClass("btn-light");

        $('.fermerVanneAir').removeClass("btn-success btn-danger");
        $('.fermerVanneAir').addClass("btn-light");
    }
}