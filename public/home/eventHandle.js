$(document).ready(() => {
    $('#consigneAirDown').click(() => {
        socket.emit("consigneAirDown", -0.1);
    });

    $('#consigneAirUp').click(() => {
        socket.emit("consigneAirUp", 0.1);
    });

    $('#consigneHumDown').click(() => {
        socket.emit("consigneHumDown", -0.1);
    });

    $('#consigneHumUp').click(() => {
        socket.emit("consigneHumUp", 0.1);
    });

    $('#consigneCo2Down').click(() => {
        socket.emit("consigneCo2Down", -100);
    });

    $('#consigneCo2Up').click(() => {
        socket.emit("consigneCo2Up", 100);
    });
});