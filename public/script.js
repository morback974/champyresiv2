let socket = null;

$(document).ready(() => {
    socket = io();
    
    socket.on("message", (data) => {
        hydrateApp(data);
    });

    socket.on("logCompteur", (logCompteur) => {
        console.log(logCompteur);
    })
});

function hydrateApp(data){
    for(let elem in data){
        if(data[elem].isClass){
            data[elem].selecteur = "." + data[elem].selecteur;
        }else{
            data[elem].selecteur = "#" + data[elem].selecteur;
        }
        $(data[elem].selecteur).text(data[elem].valeur);
    }
}