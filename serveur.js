const express = require('express');
const app = express();

const server = app.listen(3000,() => {
    console.log('Serveur Web lancer (port 3000)');
});

// Parametre les routes
require('./src/router/default')(app, express);

let client = require('./src/client');

client.config(server)
.then(() => {
    // lancement connexion carte arduino et lancement gestion champignon
    require('./src/arduino/arduino');
});
