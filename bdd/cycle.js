const con = require('./connection').con;

const insertCycle = (data) => {
    return new Promise((resolve, reject) => {
        let sql = "insert into cycle set ?";

        con.query(sql, data, (err, result) => {
            if(err){
                reject(err);
            }

            resolve({id:result.insertId, data:data});
        })
    })
}

const selectLastCycle = () => {
    return new Promise((resolve, reject) => {
        let sql = "select * from cycle order by id desc limit 1;";

        con.query(sql, (err, result) => {
            if(err){
                reject(err);
            }

            resolve(result[0]);
        })
    })
}

module.exports = {
    insertCycle: insertCycle,
    selectLastCycle: selectLastCycle
}