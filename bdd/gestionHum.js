const con = require('./connection').con;
const {arrondi} = require('./../src/util/utilitaire');

const selectLastByQuantity = (quantity = 10) => {
    return new Promise ((resolve, reject) => {
        let gestionCycle = require('./cycle');
        gestionCycle.selectLastCycle()
            .then((valeur) => {
                let sql = `select * from gestionHum left join cycle on gestionHum.idCycle = cycle.id left join actionHum on gestionHum.id = actionHum.idMesureHum where cycle.nbCycle = ${valeur.nbCycle} order by gestionHum.id desc;`;

                con.query(sql, (err, result) => {
                    resolve(result);
                })
            })
    });
}

const saveMesure = async (data) => {
    return await checkDataGestionHum(data).then((dataChecked) => {
        return insertGestionHum(dataChecked)
            .catch((err) => {
                console.log(err);
            });
    }).catch((listErreur) => {
        console.log(listErreur)
    });
}

const checkDataGestionHum = (data) => {
    return new Promise(async (resolve, reject) => {
            let listErreur = [];
            let dataChecked = {};
        try{
    
            let temperatureSec = await data.getTemperatureSec();
            if(temperatureSec < 10 || temperatureSec > 40){
                listErreur.push({code:5, "temperatureSec": `Valeur Temperature Sec incorrecte : ${temperatureSec}`});
            }else{
                dataChecked.temperatureSec = arrondi(temperatureSec);
            }
    
            let temperatureHum = await data.getTemperatureHum();
            if(temperatureHum < 10 || temperatureHum > 40){
                listErreur.push({code:6, "temperatureHum": `Valeur Temperature Hum incorrecte : ${temperatureHum}`});
            }else{
                dataChecked.temperatureHum = arrondi(temperatureHum);
            }
    
            dataChecked.tauxHumidite = data.getTauxHumidite(false);
    
            dataChecked.consigneHum = await data.getConsigneHum();
    
            dataChecked.objHum = parseFloat(await data.getObjHum());
    
            dataChecked.pasHum = parseFloat(await data.getPasHum());
    
            dataChecked.objHumActif = await data.isObjectifHumActif();
        }catch(err){
            console.log(err);
            reject(err);
        }
 

        if(listErreur.length > 0){
            reject(listErreur);
        }else{
            resolve(dataChecked);
        }
    });
}


const insertGestionHum = (data) => {
    return new Promise((resolve, reject) => {
        data.mesuredAt = new Date();
        let gestionCycle = require('./cycle');
        gestionCycle.selectLastCycle()
        .then((valeur) => {
            data.idCycle = valeur.id;
        
            let sql = "insert into gestionHum set ?";
        
            con.query(sql, data, (err, result) => {
                if(err){
                    reject(err);
                }
                resolve({id:result.insertId, data:data});
            })
        })
    })
}

module.exports = {
    selectLastByQuantity: selectLastByQuantity,
    saveMesure: saveMesure
}