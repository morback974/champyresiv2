const mysql = require('mysql');
const { resolve } = require('path');

let config = process.platform == "win32" ? {
    host: "localhost",
    user: "root",
    password: "root",
    database: "champyresi"
} : {
    host: "localhost",
    user: "root",
    database: "champyresi"
}

let con = mysql.createConnection(config);

try{
    con.connect(async (err) => {
        if(err) throw err;
        console.log("Connection Database");
    
        await tableExist('log', createTableLog);
        await tableExist('timeinterval', createTabletimeinterval);
        await tableExist('cycle', createTableCycle);
        await cycleExist();

        await tableExist('gestionAir', createTableGestionAir);
        await tableExist('actionAir', createTableActionAir);

        await tableExist('gestionHum' , createTableGestionHum);
        await tableExist('actionHum', createTableActionHum);
    });
}
catch(err){
    console.log("Erreur lors de la tentative de connexion bdd mysql");
}


function tableExist(nameTable, cb){
    let sql = `SELECT table_name FROM information_schema.tables WHERE table_schema = 'champyresi' AND table_name = '${nameTable}';`
    return launchSQL(sql).then((val) => {
        if(val.length == 0){
            return cb();
        }
    });
}

function cycleExist(){
    let sql = "select * from cycle;";
    return launchSQL(sql).then((val) => {
        if(val.length == 0){
            let insert = "insert into cycle (id, nbCycle, nbJour) VALUES (1, 1, 1);";
            return launchSQL(insert);
        }
    })
}

async function createTableLog(){
    let sql = "create table log (id int not null auto_increment primary key, dataArduino text);";
    await launchSQL(sql);
}

async function createTabletimeinterval(){
    let sql = "create table timeinterval (id int not null auto_increment primary key, libelle text, lastactivation datetime);";
    await launchSQL(sql);
}

async function createTableGestionAir(){
    let sql = "create table gestionAir (id int not null auto_increment primary key, mesuredAt datetime, temperatureAir float, consigneAir float, objAir float, pasAir float, varConsigneAirActif boolean, idCycle int);";
    await launchSQL(sql);
}

async function createTableActionAir(){
    let sql = "create table actionAir (id int not null auto_increment primary key, launchedAt datetime, deltaAir float, etatVanneFroidBefore float, etatVanneFroidAfter float, idMesureAir int);";
    await launchSQL(sql);
}

async function createTableCycle(){
    let sql = "create table cycle (id int not null auto_increment primary key, nbCycle int, nbJour int);";
    await launchSQL(sql);
}

async function createTableGestionHum(){
    let sql = "create table gestionHum (id int not null auto_increment primary key, mesuredAt datetime, temperatureSec float, temperatureHum float, tauxHumidite float, consigneHum float, objHum float, pasHum float, objHumActif boolean, idCycle int)";
    await launchSQL(sql);
}

async function createTableActionHum(){
    let sql = "create table actionHum (id int not null auto_increment primary key, launchedAt datetime, deltaHum float, tempsDeshum float, tempsBrume float, idMesureHum int)";
    await launchSQL(sql);
}

async function launchSQL(sql, data = null){
    if(!data){
        return new Promise((resolve, reject) => {
            con.query(sql, (err, result) => {
                if(err){
                    reject(err);
                } 
                resolve(result);
            })
        }).catch((err) => {
            console.log(err);
        })
    }else{
        return new Promise((resolve, reject) => {
            con.query(sql, data, (err, result) => {
                if(err){
                    reject(err);
                } 
                resolve(result);
            })
        }).catch((err) => {
            console.log(err);
        })
    }
}
module.exports = {con: con, launchSQL:launchSQL};