const con = require('./connection').con;
const {saveMesure} = require('./gestionAir');
const {arrondi} = require('./../src/util/utilitaire');

const saveAction = async (data, otherData) => {
    await saveMesure(data).then((dataInserted) => {
        checkDataActionAir(data, otherData, dataInserted).then((dataChecked) => {
            insertActionAir(dataChecked).catch((err) => {
                console.log(err);
            });
        }).catch((err) => {
            console.log(err);
        });
    }).catch((err) => {
        console.log(err);
    });
}

const checkDataActionAir = (data, otherData, dataInserted) => {
    return new Promise(async (resolve, reject) => {
        let listErreur = [];
        let dataChecked = {};

        let deltaAir = await data.getDeltaTempAir();

        if(!isNaN(deltaAir) && deltaAir <= 30){
            dataChecked.deltaAir = arrondi(deltaAir);
        }else{
            listErreur.push({"deltaAir" : `Valeur deltaAir incorrecte ${deltaAir}`});
        }

        let etatVanneFroid = data.getEtatVanneFroid();
        dataChecked.etatVanneFroidAfter = etatVanneFroid;

        if(otherData.dureeAction == 0){
            dataChecked.etatVanneFroidBefore = dataChecked.etatVanneFroidAfter;
        }
        else if(otherData.dureeAction > 0){
           etatVanneFroid -= otherData.dureeAction;
           if(etatVanneFroid < 0){
               dataChecked.etatVanneFroidBefore = 0;
           }else{
               dataChecked.etatVanneFroidBefore = etatVanneFroid;
           }
        }else{
            etatVanneFroid += otherData.dureeAction;
            if(etatVanneFroid > 40){
                dataChecked.etatVanneFroidBefore = 40;
            }else{
                dataChecked.etatVanneFroidBefore = etatVanneFroid;  
            }
        }

        dataChecked.idMesureAir = dataInserted.id;

        if(listErreur.length > 0){
            reject(listErreur);
        }else{
            resolve(dataChecked);
        }
    })
}

const insertActionAir = (data) => {
    return new Promise((resolve, reject) => {
        data.launchedAt = new Date();

        let sql = "insert into actionAir set ?";

        con.query(sql, data, (err, result) => {
            if(err){
                reject(err);
            }

            resolve({id:result.insertId, data:data});
        })
    })
}

module.exports = {
    saveAction: saveAction
}