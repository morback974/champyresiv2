const {arrondi} = require('./../src/util/utilitaire');

const con = require('./connection').con;

const saveMesure = async (data) => {
    return await checkDataGestionAir(data).then((dataChecked) => {
        return insertGestionAir(dataChecked)
            .catch((err) => {
                console.log(err);
            });
    }).catch((listErreur) => {
        
    });
}

const checkDataGestionAir = (data) => {
    return new Promise(async (resolve, reject) => {
        let listErreur = [];
        let dataChecked = {};

        let temperatureAir = await data.getTemperatureAir();
        if(temperatureAir < 10 || temperatureAir > 40){
            listErreur.push({code:1, "temperatureAir" : `Valeur Temperature Air incorrecte :${temperatureAir}`});
        }else{
            dataChecked.temperatureAir = arrondi(temperatureAir);
        }

        await data.getConsigneAir()
            .then((val) => {
                dataChecked.consigneAir = arrondi(parseFloat(val));
            })
            .catch((val) => {
                listErreur.push({code:2, "consigneAir" : `Valeur Consigne Air incorrecte :${val}`});
            });

        await data.getObjAir()
            .then((val) => {
                dataChecked.objAir = arrondi(parseFloat(val));
            })
            .catch((val) => {
                listErreur.push({code:3, "objAir" : `Valeur Objectif Air incorrecte :${val}`});
            });

        await data.getPasAir()
            .then((val) => {
                dataChecked.pasAir = arrondi(parseFloat(val));
            })
            .catch((val) => {
                listErreur.push({code:4, "pasAir" : `Valeur Pas Air incorrecte :${val}`});
            });

        dataChecked.varConsigneAirActif = data.isObjectifAirActif();

        if(listErreur.length > 0){
            reject(listErreur);
        }else{
            resolve(dataChecked);
        }
    });
}

const insertGestionAir = (data) => {
    return new Promise((resolve, reject) => {
        data.mesuredAt = new Date();
        let gestionCycle = require('./cycle');
        gestionCycle.selectLastCycle()
        .then((valeur) => {
            data.idCycle = valeur.id;
        
            let sql = "insert into gestionAir set ?";
        
            con.query(sql, data, (err, result) => {
                if(err){
                    reject(err);
                }
                resolve({id:result.insertId, data:data});
            })
        })
    })
}

const selectLastByQuantity = (quantity = null) => {
    return new Promise ((resolve, reject) => {
        let gestionCycle = require('./cycle');
        gestionCycle.selectLastCycle()
            .then((valeur) => {
                let sql = "";
                if(!quantity){
                    sql = `select * from gestionAir left join cycle on gestionAir.idCycle = cycle.id left join actionAir on gestionAir.id = actionAir.idMesureAir where cycle.nbCycle = ${valeur.nbCycle} order by gestionAir.id desc;`;
                }else{
                    sql = `select * from gestionAir left join cycle on gestionAir.idCycle = cycle.id left join actionAir on gestionAir.id = actionAir.idMesureAir where cycle.nbCycle = ${valeur.nbCycle} order by gestionAir.id desc limit ${quantity};`;
                }

                con.query(sql, (err, result) => {
                    resolve(result);
                })
            })
    });
}

module.exports = {
    saveMesure: saveMesure,
    selectLastByQuantity: selectLastByQuantity
}