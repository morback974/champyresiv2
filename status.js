const cron = require("node-cron");
const express = require("express");

const http = require('http');
const { exec } = require("child_process");

app = express();

let restartWeek = false;

// schedule tasks to be run on the server
cron.schedule("* * * * *", function() {
    console.log("running a task every minute");

http.get('http://localhost:3000/status', (resp) => {
  let data = '';

  // A chunk of data has been recieved.
  resp.on('data', (chunk) => {
    data += chunk;
  });

  // The whole response has been received. Print out the result.
  resp.on('end', () => {
    console.log(data);
  });

}).on("error", (err) => {
  console.log("Error: " + err.message);

  if(!restartWeek){
    if(process.platform == "win32"){
        exec('npm run test');
    }else{
        exec('sh launch_server.sh', {cwd : '/home/pi/Desktop/champyresi'});
        exec('sh launch_browser.sh', {cwd : '/home/pi/Desktop/champyresi'});
    }
  }
});

});
  

cron.schedule("0 0 */5 * *", function() {console.log("Error: " + restartWeek);
console.log("running a task every 5 days");
  restartWeek = true;

  setTimeout(() => {
    restartWeek = false;
  }, 300000);
});

app.listen(3128);